import cv2
import numpy as np
#from PIL import Image 
import matplotlib.pyplot as plt
import utils as ut
import time

FPS = 60
VIDEO_W = 1920
VIDEO_H = 1080
BALL_DIAMETER = int(VIDEO_H/8)
MIN_BALL_AREA = 300
EE_Y_OFFSET = 10
EPS = 40
PX_TO_M = 0.035/127

num = 1
cam = cv2.VideoCapture('media/webcam/is'+str(num)+'.mp4')
screens = int(cam.get(cv2.CAP_PROP_FRAME_COUNT))
print("screens", screens)
#print("TOTAL SCREENS", screens)
EEPos = np.empty( (2,screens) )
ballPos = np.empty( (2,screens) )
ballPos.fill(np.nan)
EEPos.fill(np.nan)
cnt = 0

while cnt < screens:
    ret, frame = cam.read()
    print("frame", cnt)

    hsvImage = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV) # HSV values are each in range 0-255 !

    # EE tool color detection
    
    # EE tool light blue color detection
    originLower = np.array([79, 134, 134])
    originUpper = np.array([85, 225, 255])
    maskOrigin = cv2.inRange(hsvImage, originLower, originUpper)
    # detect all contours
    contoursOrigin, _ = cv2.findContours(maskOrigin, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # find the largest contour, if able
    if len( contoursOrigin ) >= 1:
        largest_contour = None
        max_area = -1
        for contour in contoursOrigin:
            area = cv2.contourArea(contour)
            if area > max_area:
                max_area = area
                largest_contour = contour
        contoursOrigin = [largest_contour]
        hullOrigin = cv2.convexHull(contoursOrigin[0])
        xCorner, yCorner, wOrigin, hOrigin = cv2.boundingRect(hullOrigin)
        xOrigin = round(xCorner + wOrigin/2)
        yOrigin = round(yCorner + hOrigin/2) + EE_Y_OFFSET
        EEPos[0][cnt] = xOrigin
        EEPos[1][cnt] = yOrigin
        # draw the center point
        frame = cv2.circle(frame, (xOrigin, yOrigin), 1, (255,0,255), 3)
        cv2.drawContours(frame, contoursOrigin, -1, (0,255,0), 2 )
    else:
        print("TO-DO: no contour is found.")
        print("Number of contours:", len( contoursOrigin ))
        EEPos[0][cnt] = None
        EEPos[1][cnt] = None

    '''
    xEE, yEE = ut.find_EE_center(hsvImage, EE_Y_OFFSET, VIDEO_W, EPS)
    frame = cv2.circle(frame, (xEE, yEE), 1, (255,0,255), 3)
    EEPos[0][cnt] = xEE
    EEPos[1][cnt] = yEE
    '''

    # ball detection
    maskBall, hulls, xBall, yBall = ut.find_red_ball(hsvImage, MIN_BALL_AREA, BALL_DIAMETER)
    frame = cv2.circle(frame, (xBall, yBall), 1, (0,255,0), 3)
    ballPos[0][cnt] = xBall
    ballPos[1][cnt] = yBall

    cv2.imshow('frame', frame)
    if cv2.waitKey(15) & 0xFF == ord('q'):
        time.sleep(5)
        break

    '''
    if (cnt > 10) and ( len(hulls) != 1 ):
        time.sleep(15)

    if (cnt> 980) and (cnt < 1010):
        time.sleep(2)
    '''

    cnt += 1

cam.release()
cv2.destroyAllWindows()

t = np.linspace(0,screens/FPS,screens)
timeData = np.vstack( (np.vstack( (t, EEPos) ), ballPos) )
# Find columns that contain NaN
nan_columns = np.isnan(timeData).any(axis=0)
# Select columns that do not contain NaN
timeData = timeData[:, ~nan_columns]
print(timeData.shape)

# plotting
figure, axis = plt.subplots(2, 1)

# 1.) EE relative X pos:
relEExPos = timeData[1,:] - timeData[1,0]*np.ones( (timeData.shape[1], ) )
relEExPos = PX_TO_M* relEExPos
print(relEExPos.shape)
axis[0].scatter(timeData[0,:], relEExPos, s=2 )

# 2.) theta angle:
theta = np.arctan2(  -1*(timeData[4,:]-timeData[2,:]), timeData[3,:] - timeData[1,:]) + np.pi/2
# Identify outliers using logical indexing
outliersBig = theta>np.pi
outliersLow = theta<np.pi
# Replace outliers with the previous value
theta[outliersBig] = (np.roll(theta, 1)[outliersBig] + np.roll(theta, -1)[outliersBig] )/2
theta[outliersLow] = (np.roll(theta, 1)[outliersLow] + np.roll(theta, -1)[outliersLow] )/2 
axis[1].scatter(timeData[0,:], theta, s=2)

print(theta.shape)

#save data to csv file
timeRow = np.transpose(timeData[0,:])
print(timeRow.shape)
exportData = np.vstack( (np.vstack( (timeRow, relEExPos) ), theta) )
print(exportData.shape)
np.savetxt("project_Kendama/vision/tracking"+str(num)+".csv", np.transpose(exportData), delimiter=",")

plt.show() 
