import cv2
import numpy as np
from PIL import Image 
import matplotlib.pyplot as plt

FPS = 60

cam = cv2.VideoCapture('media/webcam/qCommand13.mp4')
screens = 16 * FPS
centers = np.empty( (2,screens) )
centers.fill(np.nan)
cnt = 0

while cnt <= screens:
    print("frame", cnt)
    ret, frame = cam.read()

    # color detection
    hsvImage = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV_FULL) # HSV values are each in range 0-255 !
    hsvTarget = np.array([107, 193, 112])
    hsvLower = hsvTarget + np.array([-10, -50, -50])
    hsvUpper = hsvTarget + np.array([10, 50, 50])
    mask = cv2.inRange(hsvImage, hsvLower, hsvUpper)

    # bounding box
    mask2 = Image.fromarray(mask)
    bbox = mask2.getbbox()
    if bbox is not None:
        x1, y1, x2, y2 = bbox
        out = cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 0, 255), 3)
        centers[0][cnt] = x2 - x1
        centers[1][cnt] = y2 - y1
    else:
        #print(None)
        out = frame

    cv2.imshow('frame', out)
    if cv2.waitKey(150) & 0xFF == ord('q'):
        break

    cnt += 1

cam.release()
cv2.destroyAllWindows()

time = np.linspace(0, 16, screens)
timeCenters = np.vstack( (time, centers) )

# Find columns that contain NaN
nan_columns = np.isnan(timeCenters).any(axis=0)

# Select columns that do not contain NaN
result = timeCenters[:, ~nan_columns]

print("result size", result.size)
print("timeCenters size", timeCenters.size)

# plotting
figure, axis = plt.subplots(2, 1)
axis[0].plot(result[0,:], result[1,:])
axis[1].plot(result[0,:], result[2,:])

plt.show() 