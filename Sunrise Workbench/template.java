package application;

import javax.inject.Inject;
import com.kuka.roboticsAPI.applicationModel.RoboticsAPIApplication;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.*;

import com.kuka.roboticsAPI.deviceModel.LBR;
import com.kuka.roboticsAPI.motionModel.PTP;

/*
 * Implementation of a robot application.
 */

public class test1 extends RoboticsAPIApplication {
	@Inject
	private LBR robot;

	@Override
	public void initialize() {
		// initialize your application here
	}

	@Override
	public void run() {
		// your application execution starts here
		getLogger().info("Hello World!");
		// move to home:
		PTP ptpToMechanicalZeroPosition = ptp(0,0,0,0,0,0,0);
		robot.move(ptpToMechanicalZeroPosition);
	}
}
