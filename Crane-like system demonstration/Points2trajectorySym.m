l0 = 0.25;  % m
g0 = 9.81;
T = 2*pi*sqrt(l0/g0)  % /s

xy = [0, 414;
    640, 414;
    640, -238;
    495, -238;
    495, -18;
    400, -18;
    400, 202;
    490, 202;
    490, 242;
    640, 242;
    640, 412;
    -142, 412;
    ]; % in mm

[coordchange, pathdiffs] = pts2paths(xy);
aMax = 400;
xStart = xy(1,1);
yStart = xy(1,2);
step = 0.01;
[traj, traj_time] = planTrajectory(T, step, aMax, xStart, yStart, coordchange, pathdiffs);

%plot(traj_time, traj(:,1))
%hold on
%plot(traj_time, traj(:,2))
%hold off
save('traj.mat','traj');
save('traj_time.mat','traj_time')


function [xChange, pathDiffs] = pts2paths(xy)
% Args: xy: Nx2 array with locations of turning points in world
%           coordinate system
% Returns: xChange: (N-1) binary array representing whether or not the
%                   change occured in the x-direction
%          pathDiffs: (N-1) array holding length of segments between
%                     turning points in mm

    % calculate xyDiff
    xyPrev = xy(1:end-1,:);
    xyNew = xy(2:end, :);
    xyDiff = xyNew - xyPrev
    % detect changes in x
    xDiff = xyDiff(:,1);
    xChange = zeros(size(xDiff));
    xChange(xDiff ~= 0) = 1;
    % calculate path lengths
    yDiff = xyDiff(:,2);
    pathDiffs = xDiff + yDiff;
end

function [traj, traj_time] = planTrajectory(T, h, a_max, x0, y0, coordChange, pathDiffs)
% Args: T : pendulum natural period
%       h: time step in sec
%       a_max: maximum acceleration in mm.s^-2
%       x0, y0: starting coordinates in mm
%       coordChange: (N x 1) binary array
%       pathlengths: lenghths of path segments in mm
%
% Returns: traj: 2D array. X, Y coordinates in time, sampled by time
%          step h.
syms t;
N = numel(pathDiffs);
vmax = a_max*T;
s0 = 0.5*a_max*T^2;
trajx = [x0];
trajy = [y0];

for i = 1:N 
   s = pathDiffs(i); 
   len = abs(s);
   if len >= 2*s0
        tr = (len - 2*s0)/vmax;
        a_use = a_max;
   else
       tr = 0;
       a_use = len/(T^2); 
   end
   
   % One-step Input Shaping
   a = a_use * heaviside(t) - a_use * heaviside(t-T);
   a = a -a_use * heaviside(t-T-tr) + a_use*heaviside(t-T-tr-T);

   % turn acceleration input signal into distance over time
   v = int(a);
   s_is = int(v);
   %subplot(N, 1, i)
   if i == 1
        %fplot(a)
   end

   % Turn Symbolic time functions into discrete data points
   t_values = 0:h:(2*T+tr);
   if s > 0
        dist = double(subs(s_is, t, t_values));
   else
        dist = (-1)*double(subs(s_is, t, t_values));
   end

   % write it in traj
   if coordChange(i) == 1           % if there is a change in x
       dist = trajx(end) + dist;
       trajx = [trajx, dist];
       dist(:) = trajy(end);
       trajy = [trajy, dist];
   else                            % if there is a change in y
      dist = trajy(end) + dist;
      trajy = [trajy, dist];
      dist(:) = trajx(end);
      trajx = [trajx, dist];
   end
end
traj = [ trajx.', trajy.'];
t_end = max(size(traj)) * h;
traj_time = 0:h:t_end;
traj_time = traj_time(1:end-1);
end