%% Using the KST to move the end effector to oa desired start position
% Author: Maros Mester
% based on the example by Mohammad SAFEEA, opyright Mohammad SAFEEA, 26th-June-2018

close all;clear;clc;
warning('off')

% Create the robot object
ip='172.31.1.147'; % The IP of the controller
arg1=KST.LBR7R800; % choose the robot iiwa7R800 or iiwa14R820
arg2=KST.Medien_Flansch_elektrisch; % choose the type of flange
Tef_flange=eye(4); % transofrm matrix of EEF with respect to flange
iiwa=KST(ip,arg1,arg2,Tef_flange); % create the object

% Start a connection with the server
flag=iiwa.net_establishConnection();
if flag==0
    return;
end
pause(1);
disp('Moving to initial position for Input Shaping demo.')
   
% move to initial position
%jPos_init={0,-0.432,0,-2.012,0,1.54,0}; % StartShaping1
%jPos_init={1.5707, 0.4223, 0, -1.8935, 0,  0.8161, 0}; %StartShaping2
jPos_init={1.5707, 0.1358, 0, -1.8926, 0,  1.1068, 0}; %StartShaping3
relVel=0.15; % relative velocity
iiwa.movePTPJointSpace(jPos_init, relVel); % point to point motion in joint space


% turn off the server
iiwa.net_turnOffServer(  );



