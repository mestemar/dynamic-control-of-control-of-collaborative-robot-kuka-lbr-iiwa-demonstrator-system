%% Shape of input force, 
clc, clear
syms t
syms theta(t)
thetadot = diff(theta, t);
thetaddot = diff(thetadot, t);
l0 = 0.25;
g0 = 9.81;
T = 2*pi*sqrt(l0/g0)
Amax = 0.5;
B = 0.;
C = 0;
D = 0;
dis =3*T;
start = 0.25;

%Simul2 - Single step
%A = Amax;
%ddx = A*heaviside(t-start) - A*heaviside(t-start-T);
%ddx = ddx -A*heaviside(t-start-dis) + A*heaviside(t-start-dis-T);

%Simul3 - 4 step
A = Amax/4;
ddx = A*heaviside(t-start) + A*heaviside(t-start-T/4) + A*heaviside(t-start-T/2) + A*heaviside(t-start-3*T/4);
ddx = ddx -A*heaviside(t-start-T) - A*heaviside(t-T-start-T/4) - A*heaviside(t-T-start-T/2) + -A*heaviside(t-T-start-3*T/4);
A = -A;
ddx = ddx + A*heaviside(t-start-dis) + A*heaviside(t-start-T/4-dis) + A*heaviside(t-start-T/2-dis) + A*heaviside(t-start-3*T/4-dis);
ddx = ddx -A*heaviside(t-start-T-dis) - A*heaviside(t-T-start-T/4-dis) - A*heaviside(t-T-start-T/2-dis) + -A*heaviside(t-T-start-3*T/4-dis);

%Simul4 - Two step
%A = Amax/2;
%ddx = A*heaviside(t-start) +A*heaviside(t-start-T/2)- A*heaviside(t-start-T) - A*heaviside(t-start-3*T/2);
%ddx = ddx - A*heaviside(t-start-dis) -A*heaviside(t-start-T/2-dis) + A*heaviside(t-start-T-dis) + A*heaviside(t-start-3*T/2-dis);

% EOM
syms l g 

eq = l*thetaddot + ddx*cos(theta) + g*sin(theta) == 0; 

% Subsitution
tspan = [0 5.5*T];
yInit = [0, 0];
l = l0;
g = g0;

eq= subs(eq);

%
[V, S] = odeToVectorField(eq);

model = matlabFunction(V,'vars',{'t','Y'});
ddxfunc = matlabFunction(ddx, 'vars', {'t'});

% Simulation
[time, Y] = ode45(model, tspan, yInit);

%% thesis plotting
fig = figure;
fsize = 18;
lnWidth = 3;

subplot(1,2,1)
t_interval = linspace( tspan(1), tspan(2) );
plot(t_interval, ddxfunc(t_interval),"Color", "black",LineWidth=lnWidth)
ax = gca; % Get the current axes
ax.FontSize = fsize-5; % Set the desired font size
title("Multi-step acceleration input shaping $\ddot{x}(t)$","FontSize",fsize, "Interpreter","latex")
xlabel('time [s]','FontSize', fsize,"Interpreter","latex" )
ylabel('$\ddot{x} \quad$ [m$\cdot$s$^{-2}$]','FontSize', fsize,"Interpreter","latex")
ylim([-Amax*1.05 Amax*1.05])
xlim(tspan)
grid on

subplot(1,2,2)
plot(time, Y(:, 1),LineWidth=lnWidth)
ax = gca; % Get the current axes
ax.FontSize = fsize -5; % Set the desired font size
title("Resulting load deviation angle $\theta(t)$","FontSize",fsize, "Interpreter","latex")
xlabel('time [s]','FontSize',fsize,"Interpreter","latex")
ylabel('$\theta \quad$ [rad]','FontSize',fsize,"Interpreter","latex")
ylim([-0.125 0.125])
xlim(tspan)
grid on

fig.Theme = "light";