%% Sending EE positions in real-time
% Author: Maros Mester
% based on example by Mohammad SAFEEA. Copyright Mohammad SAFEEA, 25th-June-2018.

close all;clear;clc;
warning('off')

% Create the robot object
ip='172.31.1.147'; % The IP of the controller
arg1=KST.LBR7R800; % choose the robot iiwa7R800 or iiwa14R820
arg2=KST.Medien_Flansch_elektrisch; % choose the type of flange
Tef_flange=eye(4); % transofrm matrix of EEF with respect to flange
iiwa=KST(ip,arg1,arg2,Tef_flange); % create the object

% Start a connection with the server
flag=iiwa.net_establishConnection();
try
    if flag==0
        return;
    end
    pause(1);

    % Get Cartesian position of EEF
    fprintf('Cartesian position')
    eefpos=iiwa.getEEFPos();
    eefposDist=eefpos;
    
    % load precalculated path
    load('traj.mat');
    load('traj_time.mat');
    time_step = traj_time(2) - traj_time(1);

    % initialize array for recorded data
    com_rate = 50;
    duration = traj_time(end);
    rec_length = round(com_rate*duration);
    recorder = zeros(rec_length, 6);
    recorder_time= zeros(1, rec_length);
    
    % Start direct servo in Cartesian space
    iiwa.realTime_startDirectServoCartesian();
    disp('Starting direct servo in Cartesian space');

    try 
        counter=1;
        initiationFlag=0;
        tic;
        t0 = toc;
        
        % Control loop
        while(toc - t0 <  traj_time(end))
            if(initiationFlag==0)
                initiationFlag=1;
                t_0=toc;
                t0=t_0;
            else
                % Perform trajectory calculation here
                index = round( (toc-t0)/time_step ) + 1;
                eefposDist{1}= traj(index, 1);
                eefposDist{2}= traj(index, 2);
  
                % Send EEF position to robot
                if(toc-t_0>0.001)
                    iiwa.sendEEfPositionf(eefposDist);
                    %recorder(counter, :) = cell2mat( iiwa.sendEEfPositionGetActualEEFpos(eefposDist) );
                    t_0=toc;
                    %recorder_time(counter) = t_0;
                    counter=counter+1;
                end
            end
        end
        tstart=t0;
        tend=toc;
        rate=counter/(tend-tstart);

    catch exception
        disp('inner catch')
        disp(['Error: ' exception.message]);
        disp(['Error identifier: ' exception.identifier]);
        disp(['Error stack: ' getReport(exception)]);
        iiwa.realTime_stopDirectServoCartesian( );
    end

    % Stop the direct servo motion
    iiwa.realTime_stopDirectServoCartesian( );
    fprintf('\nThe rate of update per second is: \n');
    disp(rate);
    fprintf('\n')
    pause(2);

catch exception
    disp('outer_catch')
    disp(['Error: ' exception.message]);
    disp(['Error identifier: ' exception.identifier]);
    disp(['Error stack: ' getReport(exception)]);
    iiwa.net_turnOffServer( );
end

% Turn off the server
iiwa.net_turnOffServer( );
warning('on')
