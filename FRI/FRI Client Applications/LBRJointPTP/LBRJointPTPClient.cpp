/**
Author: Maros Mester

\version {1.17}
*/
#include <cstdio>
#include <cstdlib> 
#include <math.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include "LBRJointPTPClient.h"
#include "friLBRState.h"

#define _USE_MATH_DEFINES
#define MAX_STEP_WIDTH 0.01     // rad
#define POS_JOINT_LIMIT 1.7453  // rad
#define NEG_JOINT_LIMIT -1.7453 // rad

using namespace KUKA::FRI;
//******************************************************************************
LBRJointPTPClient::LBRJointPTPClient(unsigned int jointMask, double qGoal[], double time, const std::string& filename)
    : _jointMask(jointMask)
    , _time(time)
    , _writeFile(filename)
{
  _ndof = KUKA::FRI::LBRState::NUMBER_OF_JOINTS;
  _pos_limit_1 = 1.9198;  // rad = 110 deg ....real limit = 120 deg
  _pos_limit_2 = 2.7925;  // rad = 160 deg ... real limit = 170 deg
  _pos_limit_3 = 2.8798;  // rad = 165 deg... real limit = 175 deg
  // initilize _q, _qGoal and _stepWidth
  printf("_q = ");
  for (int i = 0; i <= 6; i++) {
    _qPTP[i] = 0.0;
    _qGoal[i] = qGoal[i];
    _stepWidth[i] = 0;
    printf("%f, ", _qPTP[i]);
  }
  printf("\n");
}

//******************************************************************************
LBRJointPTPClient::~LBRJointPTPClient()
{
  _writeFile.close();
}
      
//******************************************************************************
// Writes data specified in the method body to a CSV file.
void LBRJointPTPClient::writeLineToCSV() {
  // read measured values from robot sensors:
  memcpy(_ipoJointPos, robotState().getIpoJointPosition(),  _ndof * sizeof(double));
  memcpy(_actualJointPos, robotState().getMeasuredJointPosition(), _ndof * sizeof(double));
  memcpy(_commandedJointPos, robotState().getCommandedJointPosition(), _ndof * sizeof(double));
  memcpy(_measuredTorque, robotState().getMeasuredTorque(), _ndof * sizeof(double));
  memcpy(_commandedTorque, robotState().getCommandedTorque(), _ndof * sizeof(double));
  memcpy(_externalTorque, robotState().getExternalTorque(), _ndof * sizeof(double));
  _perfTracking = robotState().getTrackingPerformance();

  // write measured values to writeFile
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _ipoJointPos[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _actualJointPos[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _commandedJointPos[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _measuredTorque[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _commandedTorque[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _externalTorque[i] << ",";
  }
  _writeFile << _perfTracking << ",";
  _writeFile << "\n";
}

// Sets the _qPTP values in each call of command(). To execute the motion follow this method by: robotCommand().setJointPosition(_qPTP); 
void LBRJointPTPClient::ptpJointSpace() {
  // for each joint, decide whether or not further movement is required
  for (int i= 0; i <= 6; i++)
  {
    // select appropriate joint limit
    double posLimit;
    double negLimit;
    if ( (i%2 == 1) )
    {
      posLimit = _pos_limit_1;
      negLimit = -_pos_limit_1;
    } else if ( (i%2 == 0) && (i <= 5) )
    {
      posLimit = _pos_limit_2;
      negLimit = -_pos_limit_2;
    } else
    {
      posLimit = _pos_limit_3;
      negLimit = -_pos_limit_3;
    }
    
    // check if qGoal is not already reached
    if ( ( abs(_qGoal[i] - _qPTP[i]) >= 1.5*abs(_stepWidth[i]) ) && (_qPTP[i] > negLimit) && (_qPTP[i] < posLimit))
    {
      _qPTP[i] += _stepWidth[i];      // increment _q every SampleTime
      printf("_q[%d]= %f, sw= %f ",i,  _qPTP[i], _stepWidth[i]);
    } else if ((_qPTP[i] <= negLimit) || (_qPTP[i] >= posLimit))
    {
      printf("LIMIT violation on joint %d ", i);
    } else
    {
      printf("Pos on joint %d reached ",i);
    }
  }
  printf("\n");
}

void LBRJointPTPClient::onStateChange(ESessionState oldState, ESessionState newState)
{
   LBRClient::onStateChange(oldState, newState);
   // react on state change events
   switch (newState)
   {
      case MONITORING_WAIT:
      {
        break;
      }       
      case MONITORING_READY:
      { 
        // read measured coordinates q
        memcpy(_qPTP, robotState().getMeasuredJointPosition(), LBRState::NUMBER_OF_JOINTS * sizeof(double));

        // stepWidth Calculation
        for (int i= 0; i <= 6; i++)
        {
          double calculatedStepWidth = ( (_qGoal[i] - _qPTP[i]) *  robotState().getSampleTime() )/ _time;
          if ( abs(calculatedStepWidth) <= MAX_STEP_WIDTH)
          {
            _stepWidth[i] = calculatedStepWidth;
            printf("stepWidth %d = %f rad\n", i, _stepWidth[i]);
          } else
          { 
            if (_stepWidth[i] < 0)
            {
              _stepWidth[i] = -MAX_STEP_WIDTH;
            } else
            {
              _stepWidth[i] = MAX_STEP_WIDTH;  
            }
            printf("stepWidth %d = %f rad (MAX_STEP_WIDTH)\n", i, _stepWidth[i]);
          }
        }
        break;
      }
      case COMMANDING_WAIT:
      {
        break;
      }   
      case COMMANDING_ACTIVE:
      {
        break;
      }   
      default:
      {
        break;
      }
   }
}


//******************************************************************************
void LBRJointPTPClient::monitor()
{
   LBRClient::monitor();
   
   /***************************************************************************/
   /*                                                                         */
   /*   Place user Client Code here                                           */
   /*                                                                         */
   /***************************************************************************/
   
}

//******************************************************************************
void LBRJointPTPClient::waitForCommand()
{
   // In waitForCommand(), the joint values have to be mirrored. Which is done, 
   // by calling the base method.
   LBRClient::waitForCommand();
   
   /***************************************************************************/
   /*                                                                         */
   /*   Place user Client Code here                                           */
   /*                                                                         */
   /***************************************************************************/   
   
}

//******************************************************************************
void LBRJointPTPClient::command()
{
  // write monitored values
  LBRJointPTPClient::writeLineToCSV();

  LBRJointPTPClient::ptpJointSpace();

  // In command(), the joint angle values have to be set. 
  robotCommand().setJointPosition( _qPTP );
}

#ifdef _USE_MATH_DEFINES
#undef _USE_MATH_DEFINES
#endif