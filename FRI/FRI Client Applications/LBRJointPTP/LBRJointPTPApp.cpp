/**
Author: Maros Mester

\version {1.17}
*/
#include <cstdlib>
#include <cstdio>
#include <cstring> // strstr
#include <math.h>
#include "LBRJointPTPClient.h"
#include "friUdpConnection.h"
#include "friClientApplication.h"

using namespace KUKA::FRI;


#define DEFAULT_PORTID 30200
#define DEFAULT_JOINTMASK 0x8
#define NUMBER_OF_JOINTS 7


int main (int argc, char** argv)
{

   char* hostname = NULL;
   int port =  DEFAULT_PORTID;
   unsigned int jointMask = DEFAULT_JOINTMASK;
   double qGoal[NUMBER_OF_JOINTS] = {M_PI/2, M_PI/2, M_PI/2, M_PI/2, M_PI/2, M_PI/2, M_PI/2};    //rad
   double time = 5;        //sec
   
   // create new client
   LBRJointPTPClient Client(jointMask, qGoal, time, "LBRMyMotionMonitor5sec.csv");

   /***************************************************************************/
   /*                                                                         */
   /*   Standard application structure                                        */
   /*   Configuration                                                         */
   /*                                                                         */
   /***************************************************************************/

   // create new udp connection
   UdpConnection connection;

   // pass connection and client to a new FRI client application
   ClientApplication app(connection, Client);
   
   // Connect client application to KUKA Sunrise controller.
   // Parameter NULL means: repeat to the address, which sends the data
   app.connect(port, hostname);

   /***************************************************************************/
   /*                                                                         */
   /*   Standard application structure                                        */
   /*   Execution mainloop                                                    */
   /*                                                                         */
   /***************************************************************************/

   // repeatedly call the step routine to receive and process FRI packets
   bool success = true;
   while (success)
   {
      success = app.step();
      
      // check if we are in IDLE because the FRI session was closed
      if (Client.robotState().getSessionState() == IDLE)
      {
         // We simply quit. Waiting for a FRI new session would be another 
         // possibility.
         break;
      }
   }

   /***************************************************************************/
   /*                                                                         */
   /*   Standard application structure                                        */
   /*   Dispose                                                               */
   /*                                                                         */
   /***************************************************************************/

   // disconnect from controller
   app.disconnect();
   
   return 1;
}
