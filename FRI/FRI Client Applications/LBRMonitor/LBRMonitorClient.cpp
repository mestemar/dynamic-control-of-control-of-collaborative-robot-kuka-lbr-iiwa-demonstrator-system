/**
Author: Maros Mester

*/

/**

The following license terms and conditions apply, unless a redistribution
agreement or other license is obtained by KUKA Deutschland GmbH, Augsburg, Germany.

SCOPE

The software �KUKA Sunrise.Connectivity FRI Client SDK� is targeted to work in
conjunction with the �KUKA Sunrise.Connectivity FastRobotInterface� toolkit.
In the following, the term �software� refers to all material directly
belonging to the provided SDK �Software development kit�, particularly source
code, libraries, binaries, manuals and technical documentation.

COPYRIGHT

All Rights Reserved
Copyright (C)  2014-2020 
KUKA Deutschland GmbH
Augsburg, Germany

LICENSE 

Redistribution and use of the software in source and binary forms, with or
without modification, are permitted provided that the following conditions are
met:
a) The software is used in conjunction with KUKA products only. 
b) Redistributions of source code must retain the above copyright notice, this
list of conditions and the disclaimer.
c) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the disclaimer in the documentation and/or other
materials provided with the distribution. Altered source code of the
redistribution must be made available upon request with the distribution.
d) Modification and contributions to the original software provided by KUKA
must be clearly marked and the authorship must be stated.
e) Neither the name of KUKA nor the trademarks owned by KUKA may be used to
endorse or promote products derived from this software without specific prior
written permission.

DISCLAIMER OF WARRANTY

The Software is provided "AS IS" and "WITH ALL FAULTS," without warranty of
any kind, including without limitation the warranties of merchantability,
fitness for a particular purpose and non-infringement. 
KUKA makes no warranty that the Software is free of defects or is suitable for
any particular purpose. In no event shall KUKA be responsible for loss or
damages arising from the installation or use of the Software, including but
not limited to any indirect, punitive, special, incidental or consequential
damages of any character including, without limitation, damages for loss of
goodwill, work stoppage, computer failure or malfunction, or any and all other
commercial damages or losses. 
The entire risk to the quality and performance of the Software is not borne by
KUKA. Should the Software prove defective, KUKA is not liable for the entire
cost of any service and repair.



\file
\version {1.17}
*/

#include <cstdio>
#include <cstdlib> 
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string.h>
#include "LBRMonitorClient.h"
#include "friLBRState.h"

#define _USE_MATH_DEFINES

using namespace KUKA::FRI;


//******************************************************************************
LBRMonitorClient::LBRMonitorClient(const std::string& writeFileName )
    : _writeFile(writeFileName)
{
  _ndof = KUKA::FRI::LBRState::NUMBER_OF_JOINTS;    
}

//******************************************************************************
LBRMonitorClient::~LBRMonitorClient()
{
  _writeFile.close();
}
      
//******************************************************************************
void LBRMonitorClient::onStateChange(ESessionState oldState, ESessionState newState)
{
   LBRClient::onStateChange(oldState, newState);
   // react on state change events
   switch (newState)
   {
      case MONITORING_WAIT:
      {
        break;
      }       
      case MONITORING_READY:
      { 
        break;
      }
      case COMMANDING_WAIT:
      {
        break;
      }   
      case COMMANDING_ACTIVE:
      {
        break;
      }   
      default:
      {
        break;
      }
   }
}

// Writes data specified in the method body to a CSV file.
void LBRMonitorClient::writeLineToCSV(double x, double y, double z) {
  // read measured values from robot sensors:
  memcpy(_ipoJointPos, robotState().getIpoJointPosition(),  _ndof * sizeof(double));
  //memcpy(_actualJointPos, robotState().getMeasuredJointPosition(), _ndof * sizeof(double));
  memcpy(_commandedJointPos, robotState().getCommandedJointPosition(), _ndof * sizeof(double));
  memcpy(_measuredTorque, robotState().getMeasuredTorque(), _ndof * sizeof(double));
  memcpy(_commandedTorque, robotState().getCommandedTorque(), _ndof * sizeof(double));
  memcpy(_externalTorque, robotState().getExternalTorque(), _ndof * sizeof(double));
  _perfTracking = robotState().getTrackingPerformance();

  // write measured values to writeFile
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _ipoJointPos[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _actualJointPos[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _commandedJointPos[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _measuredTorque[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _commandedTorque[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _externalTorque[i] << ",";
  }
  _writeFile << x << ", " << y << ", " << z << ", ";
  _writeFile << _perfTracking << ",";
  _writeFile << "\n";
}

// Calculate the translational vector of the EE in the base frame (forward kinematics)
Eigen::Vector3d LBRMonitorClient::FKtrans(double q[]) 
{
  double x = 0.4*cos(q[0])*sin(q[1]) - 0.152*sin(q[5])*(sin(q[4])*(1.0*cos(q[2])*sin(q[0]) + 1.0*cos(q[0])*cos(q[1])*sin(q[2]))
            + cos(q[4])*(cos(q[3])*(1.0*sin(q[0])*sin(q[2]) - cos(q[0])*cos(q[1])*cos(q[2])) - 1.0*cos(q[0])*sin(q[1])*sin(q[3])))
            + 0.4*sin(q[3])*(1.0*sin(q[0])*sin(q[2]) - cos(q[0])*cos(q[1])*cos(q[2])) 
            + 0.152*cos(q[5])*(1.0*sin(q[3])*(1.0*sin(q[0])*sin(q[2]) - cos(q[0])*cos(q[1])*cos(q[2])) 
            + 1.0*cos(q[0])*cos(q[3])*sin(q[1])) + 0.4*cos(q[0])*cos(q[3])*sin(q[1]);

  double y = 0.4*sin(q[0])*sin(q[1]) + 0.152*sin(q[5])*(sin(q[4])*(1.0*cos(q[0])*cos(q[2]) 
            - 1.0*cos(q[1])*sin(q[0])*sin(q[2])) + cos(q[4])*(cos(q[3])*(1.0*cos(q[0])*sin(q[2]) 
            + cos(q[1])*cos(q[2])*sin(q[0])) + 1.0*sin(q[0])*sin(q[1])*sin(q[3]))) 
            - 0.4*sin(q[3])*(1.0*cos(q[0])*sin(q[2]) + cos(q[1])*cos(q[2])*sin(q[0])) 
            - 0.152*cos(q[5])*(1.0*sin(q[3])*(1.0*cos(q[0])*sin(q[2]) + cos(q[1])*cos(q[2])*sin(q[0])) 
            - 1.0*cos(q[3])*sin(q[0])*sin(q[1])) + 0.4*cos(q[3])*sin(q[0])*sin(q[1]);

  double z = 0.4*cos(q[1]) + 0.152*sin(q[5])*(cos(q[4])*(1.0*cos(q[1])*sin(q[3]) - 1.0*cos(q[2])*cos(q[3])*sin(q[1]))
            + 1.0*sin(q[1])*sin(q[2])*sin(q[4])) + 0.4*cos(q[1])*cos(q[3]) + 0.152*cos(q[5])*(1.0*cos(q[1])*cos(q[3]) 
            + 1.0*cos(q[2])*sin(q[1])*sin(q[3])) + 0.4*cos(q[2])*sin(q[1])*sin(q[3]) + 0.34;

  Eigen::Vector3d transVec;
  transVec << x, y, z;
  return transVec;
} 

//******************************************************************************
void LBRMonitorClient::monitor()
{
   LBRClient::monitor();
}

//******************************************************************************
void LBRMonitorClient::waitForCommand()
{
   // In waitForCommand(), the joint values have to be mirrored. Which is done, 
   // by calling the base method.
   LBRClient::waitForCommand();
   
   /***************************************************************************/
   /*                                                                         */
   /*   Place user Client Code here                                           */
   /*                                                                         */
   /***************************************************************************/   
   
}

//******************************************************************************
void LBRMonitorClient::command()
{
  memcpy(_actualJointPos, robotState().getMeasuredJointPosition(), _ndof * sizeof(double));
  Eigen::Vector3d currentTransVec = LBRMonitorClient::FKtrans(_actualJointPos);
  double x, y, z;
  x = currentTransVec(0);
  y = currentTransVec(1);
  z = currentTransVec(2);
  LBRMonitorClient::writeLineToCSV(x, y, z);
  LBRClient::command();
}

#ifdef _USE_MATH_DEFINES
#undef _USE_MATH_DEFINES
#endif