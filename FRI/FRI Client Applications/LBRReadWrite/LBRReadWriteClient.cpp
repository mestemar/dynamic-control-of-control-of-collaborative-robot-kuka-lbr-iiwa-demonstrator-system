/**
Author: Maros Mester

*/

/**

The following license terms and conditions apply, unless a redistribution
agreement or other license is obtained by KUKA Deutschland GmbH, Augsburg, Germany.

SCOPE

The software �KUKA Sunrise.Connectivity FRI Client SDK� is targeted to work in
conjunction with the �KUKA Sunrise.Connectivity FastRobotInterface� toolkit.
In the following, the term �software� refers to all material directly
belonging to the provided SDK �Software development kit�, particularly source
code, libraries, binaries, manuals and technical documentation.

COPYRIGHT

All Rights Reserved
Copyright (C)  2014-2020 
KUKA Deutschland GmbH
Augsburg, Germany

LICENSE 

Redistribution and use of the software in source and binary forms, with or
without modification, are permitted provided that the following conditions are
met:
a) The software is used in conjunction with KUKA products only. 
b) Redistributions of source code must retain the above copyright notice, this
list of conditions and the disclaimer.
c) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the disclaimer in the documentation and/or other
materials provided with the distribution. Altered source code of the
redistribution must be made available upon request with the distribution.
d) Modification and contributions to the original software provided by KUKA
must be clearly marked and the authorship must be stated.
e) Neither the name of KUKA nor the trademarks owned by KUKA may be used to
endorse or promote products derived from this software without specific prior
written permission.

DISCLAIMER OF WARRANTY

The Software is provided "AS IS" and "WITH ALL FAULTS," without warranty of
any kind, including without limitation the warranties of merchantability,
fitness for a particular purpose and non-infringement. 
KUKA makes no warranty that the Software is free of defects or is suitable for
any particular purpose. In no event shall KUKA be responsible for loss or
damages arising from the installation or use of the Software, including but
not limited to any indirect, punitive, special, incidental or consequential
damages of any character including, without limitation, damages for loss of
goodwill, work stoppage, computer failure or malfunction, or any and all other
commercial damages or losses. 
The entire risk to the quality and performance of the Software is not borne by
KUKA. Should the Software prove defective, KUKA is not liable for the entire
cost of any service and repair.



\file
\version {1.17}
*/
#include <cstdio>
#include <cstdlib> 
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string.h>
#include "LBRReadWriteClient.h"
#include "friLBRState.h"

#define _USE_MATH_DEFINES
#define POS_LIMIT_1 2.0769 // rad = 120 deg
#define POS_LIMIT_2 2.9496  // rad = 170 deg
#define POS_LIMIT_3 3.0368 // rad = 175 deg
#define NEG_LIMIT_1 -2.0769 // rad = -120 deg
#define NEG_LIMIT_2 -2.9496  // rad = -170 deg
#define NEG_LIMIT_3 -3.0368 // rad = -175 deg

using namespace KUKA::FRI;


//******************************************************************************
LBRReadWriteClient::LBRReadWriteClient(const std::string& readFileName, const std::string& writeFileName )
    : _readFile(readFileName)
    , _writeFile(writeFileName)
{    
  _ndof = KUKA::FRI::LBRState::NUMBER_OF_JOINTS;
  _safe = true;
  _pos_limit_1 = 1.9198;  // rad = 110 deg ....real limit = 120 deg
  _pos_limit_2 = 2.7925;  // rad = 160 deg ... real limit = 170 deg
  _pos_limit_3 = 2.8798;  // rad = 165 deg... real limit = 175 deg
  for (int i = 0; i <= 6; i++) {
      _qCommand[i] = 0.0;
  }
}

//******************************************************************************
LBRReadWriteClient::~LBRReadWriteClient()
{
  _readFile.close();
  _writeFile.close();
}
      
//******************************************************************************

// Checks if the inpuy joint array is inside joint limits. Returns true if it is, false otherwise.
bool LBRReadWriteClient::checkJointLimits(double joints[]) {
   bool safe = true;
   int j = 0;
   while ( j<7 && safe)
   {
      double posLimit;
      double negLimit;
      // set the correct joint limit:
      if ( (j%2 == 1) )
      {
        posLimit = _pos_limit_1;
        negLimit = -_pos_limit_1;
      } else if ( (j%2 == 0) && (j <= 5) )
      {
        posLimit = _pos_limit_2;
        negLimit = -_pos_limit_2;
      } else
      {
        posLimit = _pos_limit_3;
        negLimit = -_pos_limit_3;
      }

      if ( (joints[j] < negLimit) || (joints[j] > posLimit) ) {
        //printf("JOINT LIMIT VIOLATION  on joint A%d!\n", j+1);
        safe = false;
        break;
      }
      j++;
   }
   return safe;
}

// Function to split a string by a delimiter and store the parts in a vector
std::vector<std::string> LBRReadWriteClient::split(const std::string &s, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (getline(tokenStream, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}

// Read a single line from _readFile, sets _qCommand.
void LBRReadWriteClient::readLineCSV() {
  std::string line;
  memcpy(_ipoJointPos, robotState().getIpoJointPosition(), _ndof * sizeof(double));
  
  if (getline(_readFile, line)) {
    // split the line into tokens using comma as the delimiter
    std::vector<std::string> tokens = LBRReadWriteClient::split(line, ',');

    // Convert each token to double and print
    int cnt = 0;
    for (const std::string &token : tokens) {
      try {
        double value = std::stod(token); // Convert string to double
        _qCommand[cnt] = value;
        //printf("_qCommand[%i] set to= %.4f\n", cnt, _qCommand[cnt]); 
      }catch (const std::invalid_argument &ex) {
        std::cerr << "Invalid argument: " << ex.what() << std::endl;
        _qCommand[cnt] = _ipoJointPos[cnt];
      }catch (const std::out_of_range &ex) {
        std::cerr << "Out of range: " << ex.what() << std::endl;
        _qCommand[cnt] = _ipoJointPos[cnt];
      }
      cnt++;
    }
  
  } else {
    // when end of file is reached, let command positions be equal to IPO positions:
    printf("EOF !");
    for (int i = 0; i <= 6; i++) {
       _qCommand[i] = _ipoJointPos[i];
    }
  }
}

void LBRReadWriteClient::onStateChange(ESessionState oldState, ESessionState newState)
{
   LBRClient::onStateChange(oldState, newState);
   // react on state change events
   switch (newState)
   {
      case MONITORING_WAIT:
      {
        break;
      }       
      case MONITORING_READY:
      { 
        break;
      }
      case COMMANDING_WAIT:
      {
        break;
      }   
      case COMMANDING_ACTIVE:
      {
        break;
      }   
      default:
      {
        break;
      }
   }
}

void LBRReadWriteClient::writeLineToCSV() {
  // read measured values from robot sensors:
  memcpy(_ipoJointPos, robotState().getIpoJointPosition(),  _ndof * sizeof(double));
  memcpy(_actualJointPos, robotState().getMeasuredJointPosition(), _ndof * sizeof(double));
  memcpy(_commandedJointPos, robotState().getCommandedJointPosition(), _ndof * sizeof(double));
  memcpy(_measuredTorque, robotState().getMeasuredTorque(), _ndof * sizeof(double));
  memcpy(_commandedTorque, robotState().getCommandedTorque(), _ndof * sizeof(double));
  memcpy(_externalTorque, robotState().getExternalTorque(), _ndof * sizeof(double));
  _perfTracking = robotState().getTrackingPerformance();

  // write measured values to writeFile
  printf("printing to file");
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _ipoJointPos[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _actualJointPos[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _commandedJointPos[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _measuredTorque[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _commandedTorque[i] << ",";
  }
  for (int i = 0; i < _ndof; i++) {
    _writeFile << _externalTorque[i] << ",";
  }
  _writeFile << _perfTracking << ",";
  _writeFile << "\n";
}

//******************************************************************************
void LBRReadWriteClient::monitor()
{
   LBRClient::monitor();
   
   /***************************************************************************/
   /*                                                                         */
   /*   Place user Client Code here                                           */
   /*                                                                         */
   /***************************************************************************/
   
}

//******************************************************************************
void LBRReadWriteClient::waitForCommand()
{
   // In waitForCommand(), the joint values have to be mirrored. Which is done, 
   // by calling the base method.
   LBRClient::waitForCommand();
   
   /***************************************************************************/
   /*                                                                         */
   /*   Place user Client Code here                                           */
   /*                                                                         */
   /***************************************************************************/   
   
}

//******************************************************************************
void LBRReadWriteClient::command()
{
  // write
  LBRReadWriteClient::writeLineToCSV();

  //read
  LBRReadWriteClient::readLineCSV();

  _safe = LBRReadWriteClient::checkJointLimits(_qCommand);

  // if even a single violation occurs, use the IPO coordinates instead 
  if (!_safe) {
    for (int i= 0; i <= 6; i++) {
      _qCommand[i] = _ipoJointPos[i];
    }
  }

  // write command data to writeFile (and print them for debugging)
  for (int i=0; i <=6; i++) {
    printf("%.2f\t", _qCommand[i]);
  }
  printf("\n");

  // In command(), the joint angle values have to be set. 
  robotCommand().setJointPosition( _qCommand );
}

#ifdef _USE_MATH_DEFINES
#undef _USE_MATH_DEFINES
#endif