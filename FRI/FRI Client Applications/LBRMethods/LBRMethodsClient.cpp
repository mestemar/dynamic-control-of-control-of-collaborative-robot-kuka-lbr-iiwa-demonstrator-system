/**
Author: Maros Mester

*/

/**

The following license terms and conditions apply, unless a redistribution
agreement or other license is obtained by KUKA Deutschland GmbH, Augsburg, Germany.

SCOPE

The software �KUKA Sunrise.Connectivity FRI Client SDK� is targeted to work in
conjunction with the �KUKA Sunrise.Connectivity FastRobotInterface� toolkit.
In the following, the term �software� refers to all material directly
belonging to the provided SDK �Software development kit�, particularly source
code, libraries, binaries, manuals and technical documentation.

COPYRIGHT

All Rights Reserved
Copyright (C)  2014-2020 
KUKA Deutschland GmbH
Augsburg, Germany

LICENSE 

Redistribution and use of the software in source and binary forms, with or
without modification, are permitted provided that the following conditions are
met:
a) The software is used in conjunction with KUKA products only. 
b) Redistributions of source code must retain the above copyright notice, this
list of conditions and the disclaimer.
c) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the disclaimer in the documentation and/or other
materials provided with the distribution. Altered source code of the
redistribution must be made available upon request with the distribution.
d) Modification and contributions to the original software provided by KUKA
must be clearly marked and the authorship must be stated.
e) Neither the name of KUKA nor the trademarks owned by KUKA may be used to
endorse or promote products derived from this software without specific prior
written permission.

DISCLAIMER OF WARRANTY

The Software is provided "AS IS" and "WITH ALL FAULTS," without warranty of
any kind, including without limitation the warranties of merchantability,
fitness for a particular purpose and non-infringement. 
KUKA makes no warranty that the Software is free of defects or is suitable for
any particular purpose. In no event shall KUKA be responsible for loss or
damages arising from the installation or use of the Software, including but
not limited to any indirect, punitive, special, incidental or consequential
damages of any character including, without limitation, damages for loss of
goodwill, work stoppage, computer failure or malfunction, or any and all other
commercial damages or losses. 
The entire risk to the quality and performance of the Software is not borne by
KUKA. Should the Software prove defective, KUKA is not liable for the entire
cost of any service and repair.



\file
\version {1.17}
*/
#include <cstdio>
#include <cstdlib> 
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string.h>
#include <Eigen/Dense>
#include "LBRMethodsClient.h"
#include "friLBRState.h"

#define _USE_MATH_DEFINES

using namespace KUKA::FRI;


//******************************************************************************
LBRMethodsClient::LBRMethodsClient(const std::string& readFileName, const std::string& writeFileName, double qGoal[], double time )
    : _readFile(readFileName)
    , _writeFile(writeFileName)
{    
    // initilize _q:
    _ndof = KUKA::FRI::LBRState::NUMBER_OF_JOINTS;
    _safe = true;
    _pos_limit_1 = 1.9198;  // rad = 110 deg ....real limit = 120 deg
    _pos_limit_2 = 2.7925;  // rad = 160 deg ... real limit = 170 deg
    _pos_limit_3 = 2.8798;  // rad = 165 deg... real limit = 175 deg
    _max_step_width = 0.01; // rad
    for (int i = 0; i <= 6; i++) {
      _qCommand[i] = 0.0;
      _qPTP[i] = 0.0;
      _qGoal[i] = qGoal[i];
      _stepWidth[i] = 0;
    }
  }

//******************************************************************************
LBRMethodsClient::~LBRMethodsClient()
{
  _readFile.close();
  _writeFile.close();
}
      
//******************************************************************************

// Checks if the inpuy joint array is inside joint limits. Returns true if it is, false otherwise.
bool LBRMethodsClient::checkJointLimits(double joints[]) {
   bool safe = true;
   int j = 0;
   while ( j<7 && safe)
   {
      double posLimit;
      double negLimit;
      // set the correct joint limit:
      if ( (j%2 == 1) )
      {
        posLimit = _pos_limit_1;
        negLimit = -_pos_limit_1;
      } else if ( (j%2 == 0) && (j <= 5) )
      {
        posLimit = _pos_limit_2;
        negLimit = -_pos_limit_2;
      } else
      {
        posLimit = _pos_limit_3;
        negLimit = -_pos_limit_3;
      }

      if ( (joints[j] < negLimit) || (joints[j] > posLimit) ) {
        //printf("JOINT LIMIT VIOLATION  on joint A%d!\n", j+1);
        safe = false;
        break;
      }
      j++;
   }
   return safe;
}

// Writes data specified in the method body to a CSV file.
void LBRMethodsClient::writeLineToCSV() {
   // read measured values from robot sensors:
   memcpy(_ipoJointPos, robotState().getIpoJointPosition(),  _ndof * sizeof(double));
   memcpy(_actualJointPos, robotState().getMeasuredJointPosition(), _ndof * sizeof(double));
   memcpy(_commandedJointPos, robotState().getCommandedJointPosition(), _ndof * sizeof(double));
   memcpy(_measuredTorque, robotState().getMeasuredTorque(), _ndof * sizeof(double));
   memcpy(_commandedTorque, robotState().getCommandedTorque(), _ndof * sizeof(double));
   memcpy(_externalTorque, robotState().getExternalTorque(), _ndof * sizeof(double));
   _perfTracking = robotState().getTrackingPerformance();

   // write measured values to writeFile
   printf("printing to file");
   for (int i = 0; i < _ndof; i++) {
     _writeFile << _ipoJointPos[i] << ",";
   }
   for (int i = 0; i < _ndof; i++) {
     _writeFile << _actualJointPos[i] << ",";
   }
   for (int i = 0; i < _ndof; i++) {
     _writeFile << _commandedJointPos[i] << ",";
   }
   for (int i = 0; i < _ndof; i++) {
     _writeFile << _measuredTorque[i] << ",";
   }
   for (int i = 0; i < _ndof; i++) {
     _writeFile << _commandedTorque[i] << ",";
   }
   for (int i = 0; i < _ndof; i++) {
     _writeFile << _externalTorque[i] << ",";
   }
   _writeFile << _perfTracking << ",";
   _writeFile << "\n";
}

// Function to split a string by a delimiter and store the parts in a vector
std::vector<std::string> LBRMethodsClient::split(const std::string &s, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (getline(tokenStream, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}

// Read a single line from _readFile, sets _qCommand.
void LBRMethodsClient::readLineCSV() {
  std::string line;
  memcpy(_ipoJointPos, robotState().getIpoJointPosition(), _ndof * sizeof(double));
  
  if (getline(_readFile, line)) {
    // split the line into tokens using comma as the delimiter
    std::vector<std::string> tokens = LBRMethodsClient::split(line, ',');

    // Convert each token to double and print
    int cnt = 0;
    for (const std::string &token : tokens) {
      try {
        double value = std::stod(token); // Convert string to double
        _qCommand[cnt] = value;
        //printf("_qCommand[%i] set to= %.4f\n", cnt, _qCommand[cnt]); 
      }catch (const std::invalid_argument &ex) {
        std::cerr << "Invalid argument: " << ex.what() << std::endl;
        _qCommand[cnt] = _ipoJointPos[cnt];
      }catch (const std::out_of_range &ex) {
        std::cerr << "Out of range: " << ex.what() << std::endl;
        _qCommand[cnt] = _ipoJointPos[cnt];
      }
      cnt++;
    }
  
  } else {
    // when end of file is reached, let command positions be equal to IPO positions:
    printf("EOF !");
    for (int i = 0; i <= 6; i++) {
       _qCommand[i] = _ipoJointPos[i];
    }
  }
}

// Sets the _qPTP values in each call of command(). To execute the motion follow this method by: robotCommand().setJointPosition(_qPTP); 
void LBRMethodsClient::ptpJointSpace() {
  // for each joint, decide whether or not further movement is required
  for (int i= 0; i <= 6; i++)
  {
    // select appropriate joint limit
    double posLimit;
    double negLimit;
    if ( (i%2 == 1) )
    {
      posLimit = _pos_limit_1;
      negLimit = -_pos_limit_1;
    } else if ( (i%2 == 0) && (i <= 5) )
    {
      posLimit = _pos_limit_2;
      negLimit = -_pos_limit_2;
    } else
    {
      posLimit = _pos_limit_3;
      negLimit = -_pos_limit_3;
    }
    
    // check if qGoal is not already reached
    if ( ( abs(_qGoal[i] - _qPTP[i]) >= 1.5*abs(_stepWidth[i]) ) && (_qPTP[i] > negLimit) && (_qPTP[i] < posLimit))
    {
      _qPTP[i] += _stepWidth[i];      // increment _q every SampleTime
      printf("_q[%d]= %f, sw= %f ",i,  _qPTP[i], _stepWidth[i]);
    } else if ((_qPTP[i] <= negLimit) || (_qPTP[i] >= posLimit))
    {
      printf("LIMIT violation on joint %d ", i);
    } else
    {
      printf("Pos on joint %d reached ",i);
    }
  }
  printf("\n");
}

// Calculate the translational vector of the EE in the base frame (forward kinematics)
Eigen::Vector3d LBRMethodsClient::FKtrans(double q[]) 
{
  double x = 0.4*cos(q[0])*sin(q[1]) - 0.152*sin(q[5])*(sin(q[4])*(1.0*cos(q[2])*sin(q[0]) + 1.0*cos(q[0])*cos(q[1])*sin(q[2]))
            + cos(q[4])*(cos(q[3])*(1.0*sin(q[0])*sin(q[2]) - cos(q[0])*cos(q[1])*cos(q[2])) - 1.0*cos(q[0])*sin(q[1])*sin(q[3])))
            + 0.4*sin(q[3])*(1.0*sin(q[0])*sin(q[2]) - cos(q[0])*cos(q[1])*cos(q[2])) 
            + 0.152*cos(q[5])*(1.0*sin(q[3])*(1.0*sin(q[0])*sin(q[2]) - cos(q[0])*cos(q[1])*cos(q[2])) 
            + 1.0*cos(q[0])*cos(q[3])*sin(q[1])) + 0.4*cos(q[0])*cos(q[3])*sin(q[1]);

  double y = 0.4*sin(q[0])*sin(q[1]) + 0.152*sin(q[5])*(sin(q[4])*(1.0*cos(q[0])*cos(q[2]) 
            - 1.0*cos(q[1])*sin(q[0])*sin(q[2])) + cos(q[4])*(cos(q[3])*(1.0*cos(q[0])*sin(q[2]) 
            + cos(q[1])*cos(q[2])*sin(q[0])) + 1.0*sin(q[0])*sin(q[1])*sin(q[3]))) 
            - 0.4*sin(q[3])*(1.0*cos(q[0])*sin(q[2]) + cos(q[1])*cos(q[2])*sin(q[0])) 
            - 0.152*cos(q[5])*(1.0*sin(q[3])*(1.0*cos(q[0])*sin(q[2]) + cos(q[1])*cos(q[2])*sin(q[0])) 
            - 1.0*cos(q[3])*sin(q[0])*sin(q[1])) + 0.4*cos(q[3])*sin(q[0])*sin(q[1]);

  double z = 0.4*cos(q[1]) + 0.152*sin(q[5])*(cos(q[4])*(1.0*cos(q[1])*sin(q[3]) - 1.0*cos(q[2])*cos(q[3])*sin(q[1]))
            + 1.0*sin(q[1])*sin(q[2])*sin(q[4])) + 0.4*cos(q[1])*cos(q[3]) + 0.152*cos(q[5])*(1.0*cos(q[1])*cos(q[3]) 
            + 1.0*cos(q[2])*sin(q[1])*sin(q[3])) + 0.4*cos(q[2])*sin(q[1])*sin(q[3]) + 0.34;

  Eigen::Vector3d transVec;
  transVec << x, y, z;
  return transVec;
} 

// Calculate the 3x3 rotarional matrix representing rotation of EE in base frame (forward kinematics)
Eigen::Matrix3d LBRMethodsClient::FKrot(double q[])
{
  double r11 = - cos(q[6])*(cos(q[5])*(sin(q[4])*(1.0*cos(q[2])*sin(q[0]) + 1.0*cos(q[0])*cos(q[1])*sin(q[2])) 
              + cos(q[4])*(cos(q[3])*(1.0*sin(q[0])*sin(q[2]) - cos(q[0])*cos(q[1])*cos(q[2])) - 1.0*cos(q[0])*sin(q[1])*sin(q[3]))) 
              + sin(q[5])*(1.0*sin(q[3])*(1.0*sin(q[0])*sin(q[2]) - cos(q[0])*cos(q[1])*cos(q[2])) + 1.0*cos(q[0])*cos(q[3])*sin(q[1]))) 
              - sin(q[6])*(1.0*cos(q[4])*(1.0*cos(q[2])*sin(q[0]) + 1.0*cos(q[0])*cos(q[1])*sin(q[2])) 
              - 1.0*sin(q[4])*(cos(q[3])*(1.0*sin(q[0])*sin(q[2]) - cos(q[0])*cos(q[1])*cos(q[2])) 
              - 1.0*cos(q[0])*sin(q[1])*sin(q[3])));

  double r21 = sin(q[6])*(1.0*cos(q[4])*(1.0*cos(q[0])*cos(q[2]) - 1.0*cos(q[1])*sin(q[0])*sin(q[2])) 
              - 1.0*sin(q[4])*(cos(q[3])*(1.0*cos(q[0])*sin(q[2]) + cos(q[1])*cos(q[2])*sin(q[0])) 
              + 1.0*sin(q[0])*sin(q[1])*sin(q[3]))) + cos(q[6])*(sin(q[5])*(1.0*sin(q[3])*(1.0*cos(q[0])*sin(q[2]) 
              + cos(q[1])*cos(q[2])*sin(q[0])) - 1.0*cos(q[3])*sin(q[0])*sin(q[1])) + cos(q[5])*(sin(q[4])*(1.0*cos(q[0])*cos(q[2]) 
              - 1.0*cos(q[1])*sin(q[0])*sin(q[2])) + cos(q[4])*(cos(q[3])*(1.0*cos(q[0])*sin(q[2]) + cos(q[1])*cos(q[2])*sin(q[0])) 
              + 1.0*sin(q[0])*sin(q[1])*sin(q[3]))));

  double r31 = - sin(q[6])*(1.0*sin(q[4])*(1.0*cos(q[1])*sin(q[3]) - 1.0*cos(q[2])*cos(q[3])*sin(q[1])) 
              - 1.0*cos(q[4])*sin(q[1])*sin(q[2])) - cos(q[6])*(sin(q[5])*(1.0*cos(q[1])*cos(q[3]) + 1.0*cos(q[2])*sin(q[1])*sin(q[3])) 
              - cos(q[5])*(cos(q[4])*(1.0*cos(q[1])*sin(q[3]) - 1.0*cos(q[2])*cos(q[3])*sin(q[1])) + 1.0*sin(q[1])*sin(q[2])*sin(q[4])));

  double r12 = - sin(q[6])*(1.0*sin(q[4])*(1.0*cos(q[1])*sin(q[3]) - 1.0*cos(q[2])*cos(q[3])*sin(q[1])) - 1.0*cos(q[4])*sin(q[1])*sin(q[2]))
              - cos(q[6])*(sin(q[5])*(1.0*cos(q[1])*cos(q[3]) + 1.0*cos(q[2])*sin(q[1])*sin(q[3])) 
              - cos(q[5])*(cos(q[4])*(1.0*cos(q[1])*sin(q[3]) - 1.0*cos(q[2])*cos(q[3])*sin(q[1])) 
              + 1.0*sin(q[1])*sin(q[2])*sin(q[4])));

  double r22 = cos(q[6])*(1.0*cos(q[4])*(1.0*cos(q[0])*cos(q[2]) - 1.0*cos(q[1])*sin(q[0])*sin(q[2])) 
              - 1.0*sin(q[4])*(cos(q[3])*(1.0*cos(q[0])*sin(q[2]) + cos(q[1])*cos(q[2])*sin(q[0])) + 1.0*sin(q[0])*sin(q[1])*sin(q[3]))) 
              - 1.0*sin(q[6])*(sin(q[5])*(1.0*sin(q[3])*(1.0*cos(q[0])*sin(q[2]) + cos(q[1])*cos(q[2])*sin(q[0])) 
              - 1.0*cos(q[3])*sin(q[0])*sin(q[1])) + cos(q[5])*(sin(q[4])*(1.0*cos(q[0])*cos(q[2]) 
              - 1.0*cos(q[1])*sin(q[0])*sin(q[2])) + cos(q[4])*(cos(q[3])*(1.0*cos(q[0])*sin(q[2]) + cos(q[1])*cos(q[2])*sin(q[0])) 
              + 1.0*sin(q[0])*sin(q[1])*sin(q[3]))));

  double r32 = 1.0*sin(q[6])*(sin(q[5])*(1.0*cos(q[1])*cos(q[3]) + 1.0*cos(q[2])*sin(q[1])*sin(q[3])) 
              - cos(q[5])*(cos(q[4])*(1.0*cos(q[1])*sin(q[3]) - 1.0*cos(q[2])*cos(q[3])*sin(q[1])) + 1.0*sin(q[1])*sin(q[2])*sin(q[4])))
              - cos(q[6])*(1.0*sin(q[4])*(1.0*cos(q[1])*sin(q[3]) - 1.0*cos(q[2])*cos(q[3])*sin(q[1])) - 1.0*cos(q[4])*sin(q[1])*sin(q[2]));

  double r13 = 1.0*cos(q[5])*(1.0*sin(q[3])*(1.0*sin(q[0])*sin(q[2]) - cos(q[0])*cos(q[1])*cos(q[2])) + 1.0*cos(q[0])*cos(q[3])*sin(q[1])) 
              - 1.0*sin(q[5])*(sin(q[4])*(1.0*cos(q[2])*sin(q[0]) + 1.0*cos(q[0])*cos(q[1])*sin(q[2])) 
              + cos(q[4])*(cos(q[3])*(1.0*sin(q[0])*sin(q[2]) - cos(q[0])*cos(q[1])*cos(q[2])) - 1.0*cos(q[0])*sin(q[1])*sin(q[3])));

  double r23 = 1.0*sin(q[5])*(sin(q[4])*(1.0*cos(q[0])*cos(q[2]) - 1.0*cos(q[1])*sin(q[0])*sin(q[2])) 
              + cos(q[4])*(cos(q[3])*(1.0*cos(q[0])*sin(q[2]) + cos(q[1])*cos(q[2])*sin(q[0])) + 1.0*sin(q[0])*sin(q[1])*sin(q[3]))) 
              - 1.0*cos(q[5])*(1.0*sin(q[3])*(1.0*cos(q[0])*sin(q[2]) + cos(q[1])*cos(q[2])*sin(q[0])) 
              - 1.0*cos(q[3])*sin(q[0])*sin(q[1]));

  double r33 = 1.0*sin(q[5])*(cos(q[4])*(1.0*cos(q[1])*sin(q[3]) - 1.0*cos(q[2])*cos(q[3])*sin(q[1])) 
              + 1.0*sin(q[1])*sin(q[2])*sin(q[4])) + 1.0*cos(q[5])*(1.0*cos(q[1])*cos(q[3]) + 1.0*cos(q[2])*sin(q[1])*sin(q[3]));

  Eigen::Vector3d v1(r11, r12, r13);
  Eigen::Vector3d v2(r21, r22, r23);
  Eigen::Vector3d v3(r31, r32, r33);
  Eigen::Matrix3d rotMat;
  rotMat.row(0) = v1;
  rotMat.row(1) = v2;
  rotMat.row(2) = v3;
  return rotMat;
}

// Calculate the full 4x4 transformation matrix from EE to base frame  (forward kinematics)
Eigen::Matrix4d LBRMethodsClient::FK(double q[])
{
  Eigen::Matrix3d R = LBRMethodsClient::FKrot(q);
  Eigen::Vector3d t = LBRMethodsClient::FKtrans(q);
  Eigen::Matrix4d T;
  T.block<3, 3>(0, 0) = R;    // 3D rotation
  T(0,3) = t(0);              //x
  T(1,3) = t(1);              //y
  T(2,3) = t(2);              //z
  Eigen::RowVector4d vec;
  vec << 0, 0, 0, 1;
  T.row(3) = vec;
  return T;
}

void LBRMethodsClient::onStateChange(ESessionState oldState, ESessionState newState)
{
   LBRClient::onStateChange(oldState, newState);
   // react on state change events
   switch (newState)
   {
      case MONITORING_WAIT:
      {
        break;
      }       
      case MONITORING_READY:
      { 
        // read measured coordinates q
        memcpy(_qPTP, robotState().getMeasuredJointPosition(), LBRState::NUMBER_OF_JOINTS * sizeof(double));

        // stepWidth Calculation
        for (int i= 0; i <= 6; i++)
        {
          double calculatedStepWidth = ( (_qGoal[i] - _qPTP[i]) *  robotState().getSampleTime() )/ _time;
          if ( abs(calculatedStepWidth) <= _max_step_width)
          {
            _stepWidth[i] = calculatedStepWidth;
            printf("stepWidth %d = %f rad\n", i, _stepWidth[i]);
          } else
          { 
            if (_stepWidth[i] < 0)
            {
              _stepWidth[i] = -_max_step_width;
            } else
            {
              _stepWidth[i] = _max_step_width;  
            }
            printf("stepWidth %d = %f rad (MAX_STEP_WIDTH)\n", i, _stepWidth[i]);
          }
        }
        break;
      }
      case COMMANDING_WAIT:
      {
        break;
      }   
      case COMMANDING_ACTIVE:
      {
        break;
      }   
      default:
      {
        break;
      }
   }
}

//******************************************************************************
void LBRMethodsClient::monitor()
{
   LBRClient::monitor();
   
   /***************************************************************************/
   /*                                                                         */
   /*   Place user Client Code here                                           */
   /*                                                                         */
   /***************************************************************************/
   
}

//******************************************************************************
void LBRMethodsClient::waitForCommand()
{
   // In waitForCommand(), the joint values have to be mirrored. Which is done, 
   // by calling the base method.
   LBRClient::waitForCommand();
   
   /***************************************************************************/
   /*                                                                         */
   /*   Place user Client Code here                                           */
   /*                                                                         */
   /***************************************************************************/   
   
}

//******************************************************************************
void LBRMethodsClient::command()
{
  LBRClient::command();
}

#ifdef _USE_MATH_DEFINES
#undef _USE_MATH_DEFINES
#endif