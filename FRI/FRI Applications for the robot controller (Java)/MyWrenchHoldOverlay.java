package com.kuka.connectivity.fri.example;

import static com.kuka.roboticsAPI.motionModel.BasicMotions.ptp;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.kuka.connectivity.fastRobotInterface.ClientCommandMode;
import com.kuka.connectivity.fastRobotInterface.FRIConfiguration;
import com.kuka.connectivity.fastRobotInterface.FRIJointOverlay;
import com.kuka.connectivity.fastRobotInterface.FRISession;
import com.kuka.roboticsAPI.applicationModel.RoboticsAPIApplication;
import com.kuka.roboticsAPI.controllerModel.Controller;
import com.kuka.roboticsAPI.deviceModel.LBR;
import com.kuka.roboticsAPI.geometricModel.CartDOF;
import com.kuka.roboticsAPI.motionModel.PositionHold;
import com.kuka.roboticsAPI.motionModel.controlModeModel.CartesianImpedanceControlMode;

/**
 * Creates a FRI Session.
 */
public class MyWrenchHoldOverlay extends RoboticsAPIApplication
{
    private Controller _lbrController;
    private LBR _lbr;
    private String _clientName;

    @Override
    public void initialize()
    {
        _lbrController = (Controller) getContext().getControllers().toArray()[0];
        _lbr = (LBR) _lbrController.getDevices().toArray()[0];
        // **********************************************************************
        // *** change next line to the FRIClient's IP address                 ***
        // **********************************************************************
        _clientName = "192.170.10.3";
    }

    @Override
    public void run()
    {
        // move to start pose
        _lbr.move(ptp(0, 0.071, 0, -1.3523, 0, 0.1301, 0 ).setJointVelocityRel(0.25));	// radians
    	
        // configure and start FRI session
        FRIConfiguration friConfiguration = FRIConfiguration.createRemoteConfiguration(_lbr, _clientName);
        friConfiguration.setSendPeriodMilliSec(5);

        getLogger().info("Creating FRI connection to " + friConfiguration.getHostName());
        getLogger().info("SendPeriod: " + friConfiguration.getSendPeriodMilliSec() + "ms |"
                + " ReceiveMultiplier: " + friConfiguration.getReceiveMultiplier());
        
        //Session
        FRISession friSession = new FRISession(friConfiguration);
        //overlay
        FRIJointOverlay overlay = new FRIJointOverlay (friSession, ClientCommandMode.WRENCH);
       
        
        // wait until FRI session is ready to switch to command mode
        try
        {
            friSession.await(10, TimeUnit.SECONDS);
        }
        catch (final TimeoutException e)
        {
            getLogger().error(e.getLocalizedMessage());
            friSession.close();
            return;
        }
        
        getLogger().info("FRI connection established.");
        
        //control mode
        CartesianImpedanceControlMode ctrMode = new CartesianImpedanceControlMode();
        ctrMode.parametrize(CartDOF.ALL).setStiffness(1);
        ctrMode.parametrize(CartDOF.ALL).setDamping(1);
        //move type
        PositionHold posHold = new PositionHold(ctrMode, 4000, TimeUnit.MILLISECONDS);
        
        // position hold
        _lbr.move(posHold.addMotionOverlay(overlay));

        // done
        friSession.close();
        getLogger().info("FRI connection terminated.");
    }

    /**
     * main.
     * 
     * @param args
     *            args
     */
    public static void main(final String[] args)
    {
        final MyWrenchHoldOverlay app = new MyWrenchHoldOverlay();
        app.runApplication();
    }

}
