import rclpy
from rclpy.node import Node
from lbr_fri_msgs.msg import LBRState

class CustomSubscriber(Node):
    def __init__(self):
        super().__init__('custom_subscriber')
        self.subscription = self.create_subscription(
            LBRState,
            "/lbr/state",
            self.custom_callback,
            1
        )
        self.subscription  # prevent unused variable warning

    def custom_callback(self, msg):
        for i, value in enumerate(msg.measured_joint_position):
            self.get_logger().info("Joint coord[%d]: %.2f" % (i, value))

def main(args=None):
    rclpy.init(args=args)
    subscriber = CustomSubscriber()
    rclpy.spin(subscriber)
    subscriber.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()