import rclpy
from rclpy.node import Node
from lbr_fri_msgs.msg import LBRPositionCommand, LBRState
import numpy as np

LIMIT_1 = 2.0769  # rad = 120 deg
LIMIT_2 = 2.9496  # rad = 170 deg
LIMIT_3 = 3.0368  # rad = 175 deg
FILE_NAME = "/home/strojovna/kuka-lbr-iiwa/FRI/FRI-Client-SDK-Cpp/bin/qCommand12.csv"

class CustomJointPositionOverlayNode(Node):
    def __init__(self, read_file: str)-> None:
        super().__init__('joint_position_commander')

        self.trajectory = np.genfromtxt(read_file, delimiter=',')
        self.num_of_commands = self.trajectory.shape[0]
        self.cnt = 0
        self.lbr_position_command = LBRPositionCommand()

        # create publisher to /lbr/command/joint_position
        self.lbr_position_command_pub = self.create_publisher(
            LBRPositionCommand,
            "/lbr/command/joint_position",
            1
        )
        
        # create subscription to /lbr/state
        self.lbr_state_sub = self.create_subscription(
            LBRState,
            "/lbr/state",
            self.on_lbr_state,
            1
        )
    
    def check_commad(self, command) -> bool:
        upper_limits = np.array([LIMIT_2, LIMIT_1, LIMIT_2, LIMIT_1, LIMIT_2, LIMIT_1, LIMIT_3])
        lower_limits = -1*upper_limits
        within_limits = np.logical_and(command > lower_limits, command < upper_limits)
        return bool( np.all(within_limits) )

    def on_lbr_state(self, lbr_state: LBRState) -> None:
        if (lbr_state.session_state == 4) and (self.cnt < self.num_of_commands):  # KUKA::FRI::COMMANDING_ACTIVE == 4
            # get trajectory data
            command = self.trajectory[self.cnt,:]

            # check the data, replace them if they are outside limits
            if not ( self.check_commad(command) ):
                command = lbr_state.measured_joint_position
                print("Supplied data violate joint limits!")
        
            self.lbr_position_command.joint_position = command
            print("sending", command)
            self.lbr_position_command_pub.publish(self.lbr_position_command)
            self.cnt += 1

                    
def main(args=None) -> None:
    rclpy.init(args=args)
    rclpy.spin( CustomJointPositionOverlayNode(FILE_NAME) )
    rclpy.shutdown()