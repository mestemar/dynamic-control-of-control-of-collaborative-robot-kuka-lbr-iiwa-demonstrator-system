/**
Author: Maros Mester

*/
/**

The following license terms and conditions apply, unless a redistribution
agreement or other license is obtained by KUKA Deutschland GmbH, Augsburg, Germany.

SCOPE

The software �KUKA Sunrise.Connectivity FRI Client SDK� is targeted to work in
conjunction with the �KUKA Sunrise.Connectivity FastRobotInterface� toolkit.
In the following, the term �software� refers to all material directly
belonging to the provided SDK �Software development kit�, particularly source
code, libraries, binaries, manuals and technical documentation.

COPYRIGHT

All Rights Reserved
Copyright (C)  2014-2020 
KUKA Deutschland GmbH
Augsburg, Germany

LICENSE 

Redistribution and use of the software in source and binary forms, with or
without modification, are permitted provided that the following conditions are
met:
a) The software is used in conjunction with KUKA products only. 
b) Redistributions of source code must retain the above copyright notice, this
list of conditions and the disclaimer.
c) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the disclaimer in the documentation and/or other
materials provided with the distribution. Altered source code of the
redistribution must be made available upon request with the distribution.
d) Modification and contributions to the original software provided by KUKA
must be clearly marked and the authorship must be stated.
e) Neither the name of KUKA nor the trademarks owned by KUKA may be used to
endorse or promote products derived from this software without specific prior
written permission.

DISCLAIMER OF WARRANTY

The Software is provided "AS IS" and "WITH ALL FAULTS," without warranty of
any kind, including without limitation the warranties of merchantability,
fitness for a particular purpose and non-infringement. 
KUKA makes no warranty that the Software is free of defects or is suitable for
any particular purpose. In no event shall KUKA be responsible for loss or
damages arising from the installation or use of the Software, including but
not limited to any indirect, punitive, special, incidental or consequential
damages of any character including, without limitation, damages for loss of
goodwill, work stoppage, computer failure or malfunction, or any and all other
commercial damages or losses. 
The entire risk to the quality and performance of the Software is not borne by
KUKA. Should the Software prove defective, KUKA is not liable for the entire
cost of any service and repair.



\file
\version {1.17}
*/
#include <cstdio>
#include <cstdlib> 
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string.h>
#include <string>
#include <unistd.h>
#include <thread>
#include <mutex>
#include <termios.h>
#include <iomanip>
#include <cmath>
#include <math.h>
#include <fcntl.h>
#include "LBRPendulumFeedback.h"
#include "friLBRState.h"

#define _USE_MATH_DEFINES
using namespace KUKA::FRI;
std::mutex VarMtx;
double angle = 0;
double vel= 0;


//******************************************************************************
LBRPendulumFeedback::LBRPendulumFeedback(const std::string& writeFileName, const std::string& portName, const std::string& lineDataFileName, int numCommands)
  : _writeFile(writeFileName), 
    _portName(portName),
    _lineDataFile(lineDataFileName),
    _numCommands(numCommands)    
{
  _ndof = KUKA::FRI::LBRState::NUMBER_OF_JOINTS;
  _safe = true;
  _pos_limit_1 = 1.9198; // rad = 110 deg ....real limit = 120 deg
  _pos_limit_2 = 2.7925;  // rad = 160 deg ... real limit = 170 deg
  _pos_limit_3 = 2.8798;// rad = 165 deg... real limit = 175 deg
  _jPosCommand[0] = 0;
  _jPosCommand[1] = 0.071;
  _jPosCommand[2] = 0;
  _jPosCommand[3] = -1.3523;
  _jPosCommand[4] = 0;
  _jPosCommand[5] = 0.1301;
  _jPosCommand[6] = 0;


  // open the serial port specified by _portName
  _serial_fd = LBRPendulumFeedback::openSerialPort();

  // Start the reader thread
  _keepReading = true;
  std::cout << "Start reader thread" << std::endl;
  _readerThread = std::thread(&LBRPendulumFeedback::SerialReader, this);
  sleep(2);
  std::cout << "Main(constructor) stopped sleeping" << std::endl;

}

//******************************************************************************
LBRPendulumFeedback::~LBRPendulumFeedback()
{
  _writeFile.close();
  // end the reader thread
  _keepReading = false;
  _readerThread.join();
  std::cout << "Reader thread finished." << std::endl;  
}

// function that opens the computer's serial port specified in _portName
int LBRPendulumFeedback::openSerialPort() {
    int fd = open(_portName.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1) {
        std::cerr << "Failed to open port " << _portName << std::endl;
        return -1;
    }

    termios tty;
    memset(&tty, 0, sizeof tty);

    if (tcgetattr(fd, &tty) != 0) {
        std::cerr << "Error from tcgetattr: " << strerror(errno) << std::endl;
        close(fd);
        return -1;
    }

    cfsetospeed(&tty, B9600);
    cfsetispeed(&tty, B9600);

    tty.c_cflag &= ~PARENB; // no parity bit
    tty.c_cflag &= ~CSTOPB; // 1 stopbit
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;     // 8 bits per byte

    tty.c_cflag &= ~CRTSCTS;       //no hardware flow control
    tty.c_cflag |= CREAD | CLOCAL; // turn on READ t

    tty.c_lflag &= ~ICANON;
    tty.c_lflag &= ~ECHO;    //disable echo
    tty.c_lflag &= ~ECHOE;   //disable erasure
    tty.c_lflag &= ~ECHONL;  //disable new-line echo
    tty.c_lflag &= ~ISIG;  

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl
    tty.c_iflag &= ~(IGNBRK|BRKINT|ISTRIP|INLCR|IGNCR|ICRNL); // disable any special handling of received bytes

    tty.c_oflag &= ~OPOST; // prevent special interpretation of output bytes e.g. \n

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        std::cerr << "Error from tcsetattr: " << strerror(errno) << std::endl;
        close(fd);
        return -1;
    }

    return fd;
}

// Function that serves as the secondary thread for reading the serial port data
void LBRPendulumFeedback::SerialReader() {
    char buf[256];
    std::string data;
    std::cout << "SerialReader start." << std::endl;

    while (_keepReading) {
        ssize_t n = read(_serial_fd, buf, sizeof(buf) - 1);
        if (n > 0) {
            buf[n] = '\0';
            data.append(buf);

            size_t pos;
            while ((pos = data.find('\n')) != std::string::npos) {
                std::string line = data.substr(0, pos);
                size_t comma_pos = line.find(',');
                // LOCKING the access to shared variables
                VarMtx.lock();       
                if (comma_pos != std::string::npos) {
                    try {
                        angle = std::stod(line.substr(0, comma_pos));
                        vel = std::stod(line.substr(comma_pos + 1));
                    } catch (const std::exception& e) {
                        std::cerr << "Error parsing line: " << e.what() << std::endl;
                    }
                }
                VarMtx.unlock();
                // UNLOCKING the access to shared variables
                data.erase(0, pos + 1);
            }
        }
    }
} 

// convert the angle to be in <0, 2*PI> and start with 0 rad when pendulum is hanging down
double LBRPendulumFeedback::processAngle(double rawAngle) {
  return -1* std::fmod( (rawAngle + 2.2), 2*M_PI);
}

// Writes the FRI-monitored values into a CSV file
void LBRPendulumFeedback::write(double angleCopy, double velCopy) {
   // read measured values from robot sensors:
   _perfTracking = robotState().getTrackingPerformance();

   // write measured values to writeFile
   for (int i = 0; i < _ndof; i++) {
     _writeFile << _actualJointPos[i] << ",";
   }
   for (int i = 0; i < _ndof; i++) {
      _writeFile << _jPosCommand[i] << ",";
   } 
   _writeFile << _perfTracking << ",";
   _writeFile << angleCopy << ",";
   _writeFile << velCopy << ",";
   _writeFile << "\n";
}

//******************************************************************************
void LBRPendulumFeedback::onStateChange(ESessionState oldState, ESessionState newState)
{
   LBRClient::onStateChange(oldState, newState);
   // react on state change events
   switch (newState)
   {
      case MONITORING_WAIT:
      {
        break;
      }       
      case MONITORING_READY:
      { 
        break;
      }
      case COMMANDING_WAIT:
      {
        break;
      }   
      case COMMANDING_ACTIVE:
      {
        break;
      }   
      default:
      {
        break;
      }
   }
}

//******************************************************************************
void LBRPendulumFeedback::monitor()
{
   LBRClient::monitor();
}

//******************************************************************************
void LBRPendulumFeedback::waitForCommand()
{
   // In waitForCommand(), the joint values have to be mirrored. Which is done, 
   // by calling the base method.
   LBRClient::waitForCommand();
   
   /***************************************************************************/
   /*                                                                         */
   /*   Place user Client Code here                                           */
   /*                                                                         */
   /***************************************************************************/   
   
}

//******************************************************************************
void LBRPendulumFeedback::command()
{
  double angleLocal, velLocal;

  // printing Values from serial port
  // LOCKING the access to shared variables
  VarMtx.lock();
  angleLocal = angle;
  velLocal = vel;
  VarMtx.unlock();
  // UNLOCKING the access to shared variables

  // processing the encoder values so that they match model
  angleLocal = LBRPendulumFeedback::processAngle(angleLocal);
  velLocal = -1*velLocal;
  std::cout << _commandCnt << ", Angle: " << angleLocal << ", Ang. Vel=" << velLocal << ", ";

  
  LBRClient::command();
  LBRPendulumFeedback::write(angleLocal, velLocal);
}

#ifdef _USE_MATH_DEFINES
#undef _USE_MATH_DEFINES
#endif