#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include "AS5600.h"

#define INT_TO_RAD 0.001534
AS5600 as5600;   //  use default Wire

RF24 radio(7,8); // CE, CSN

const byte address[6] = "00001";
double angle = 0;
double vel = 0;

void setup() {
  Serial.begin(9600);

  // radio setup:
  radio.begin();
  radio.openWritingPipe(address);
  radio.setPALevel(RF24_PA_MIN);
  Serial.print("radio ok");

  // AS5600 sensor setup:
  Wire.begin();
  as5600.begin();  //  set direction pin.
  as5600.setDirection(AS5600_CLOCK_WISE);  //  default, just be explicit.
  Serial.println(as5600.getAddress());
  int b = as5600.isConnected();
  Serial.print("Connect: ");
  Serial.println(b);

  //  set encoder's initial position:
  delay(1000);
  as5600.getCumulativePosition();
  as5600.getAngularSpeed(AS5600_MODE_RADIANS);

}

void loop() {
  // read encoder:
  angle = INT_TO_RAD * as5600.getCumulativePosition();    // angle in radians
  vel = as5600.getAngularSpeed(AS5600_MODE_RADIANS);      // angular velocity in rad.s^-1
  // prepare for transmission:
  byte data[sizeof(double) * 2];
  memcpy(&data[0], &angle, sizeof(double));
  memcpy(&data[sizeof(double)], &vel, sizeof(double) );
  // send:
  radio.write(&data, sizeof(data));
  // also print here:
  Serial.print(angle);
  Serial.println(vel);
  delay(5);
}