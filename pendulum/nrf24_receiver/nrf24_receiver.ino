/*
* Maros Mester
* Library: TMRh20/RF24, https://github.com/tmrh20/RF24/
*/

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); // CE, CSN

const byte address[6] = "00001";

void setup() {
  Serial.begin(9600);
  Serial.println("start");
  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();
}

void loop() {
  if (radio.available()) {
    double angle, vel;
    byte data[sizeof(double)*2];
    radio.read(&data, sizeof(data));

    memcpy(&angle, &data[0], sizeof(double) );
    memcpy(&vel, &data[sizeof(double)], sizeof(double));

    Serial.print(angle);
    Serial.print(",");
    Serial.println(vel);
    delay(1);
  }
  //Serial.println("not avalaible");
}