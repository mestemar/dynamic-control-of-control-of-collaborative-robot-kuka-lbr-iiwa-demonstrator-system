%% CupAndBall
clc, clear

% variable definitions
g = 9.81;
l = 0.15;
m = 0.0139;   % small= 5.3 g, big = 13.96g 
theta_desired = deg2rad(115);
Ed = m*g*l*cos(theta_desired) + m*g*l;
k = 9;
kp = 0;
kd= 0;
syms t
timeEnd = 11.3; % sec
theta_init = [0, 0]; % theta, dtheta
s_init = [0 0];

% mode1-to-mode2 transition only:
disp('Entering mode 1')
[tMode1, thetaRes, tEvent1, thetaEvent, timeDds] = CABMode1_energy([0, timeEnd], theta_init, s_init, g, l, m, Ed , k, kp, kd);
disp('Exiting mode 1')

% gather the tTotal,x,y,dx,dy,s,ds variables
tTotal = tMode1;
s = thetaRes(:,3);
ds = thetaRes(:,4);
[x, y] = theta2xy(thetaRes(:,1), s, l);
[dx, dy] = dtheta2velocity(thetaRes(:,2), thetaRes(:,1), l);

% Mechanical energy evaluation
E = m*g*y + 0.5*m*(dx.^2 + dy.^2);
% mass-to-origin distance check
dist = sqrt((s-x).^2 + (0-y).^2);

subplot(4,2,1)
plot(tMode1, thetaRes(:,1));
grid on
title('mass angle');
subplot(4,2,2);
plot(tMode1, thetaRes(:,2));
title('mass velocity');
grid on
subplot(4,2,3);
plot(tTotal, s);
title('origin position');
grid on
subplot(4,2,4);
plot(tTotal, ds);
grid on
title('origin velocity');
subplot(4,2,5);
plot(timeDds(1,:), timeDds(2,:));
grid on
title('origin acceleration');
subplot(4,2,7)
plot(tTotal, E);
title("total energy")
grid on
subplot(4,2,8)
plot(tTotal, dist)
title("mass-origin distance")


%% thesis plotting
fig = figure;

fsize = 18;
lnWidth = 2;
% set plot line width higher

subplot(2,2,1)
plot(tTotal, E, "LineWidth",lnWidth);
title("Total ball energy $E(t)$", "Interpreter","latex", "FontSize",fsize)
ylabel("$E$ [J]","Interpreter","latex", "FontSize",fsize)
xlabel("time [s]","Interpreter","latex","FontSize",fsize)
grid on

subplot(2,2,2);
plot(timeDds(1,:), timeDds(2,:), "LineWidth",lnWidth);
grid on
title("End effector acceleration $\ddot{x}(t)$", "Interpreter","latex", "FontSize",fsize)
ylabel(" $\ddot{x}$ [m.s$^{-2}$]","Interpreter","latex", "FontSize",fsize)
xlabel("time [s]","Interpreter","latex","FontSize",fsize)

subplot(2,2,3)
plot(tMode1, thetaRes(:,1), "LineWidth",lnWidth);
title("Ball angle", "Interpreter","latex", "FontSize",fsize)
ylabel("$\theta$ [rad]","Interpreter","latex", "FontSize",fsize)
xlabel("time [s]","Interpreter","latex","FontSize",fsize)
grid on


subplot(2,2,4);
plot(tTotal, s, "LineWidth",lnWidth);
title("End effector relative position $x(t)$", "Interpreter","latex", "FontSize",fsize)
ylabel("$x$ [m]","Interpreter","latex", "FontSize",fsize)
xlabel("time [s]","Interpreter","latex","FontSize",fsize)
grid on

fig.Theme = "light"; 