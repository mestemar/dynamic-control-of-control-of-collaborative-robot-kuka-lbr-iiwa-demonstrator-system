function [tNew, sNew] = resampleArray(t, s, newStep)
% resamples the array s in time t such that a new array sNew has the exact
% step size defined by newStep
    
    % transpose if necessary
    if size(t, 1) == 1
        t = t.';
    end
    if size(s, 1) == 1
        s = s.';
    end
    
    %generate new time array
    tNew = 0:newStep:( t(end) -newStep);
    numNewSamples = numel(tNew);
    sNew = NaN(numNewSamples, 1);
    
    % resample
    for i=1:numNewSamples
        [~, index] = min(abs(t-tNew(i)));
        
        if t(index) - tNew(i) > 0 && i ~= 1
            timeRatio =  (t(index) - tNew(i)) / ( t(index) - t(index-1) );
            sNew(i) = s(index-1) + (1-timeRatio)*( s(index) - s(index-1) );
        elseif t(index) - tNew(i) < 0 && i ~= numNewSamples
            timeRatio = ( tNew(i) - t(index) ) / ( t(index + 1) - t(index) );
            sNew(i) = s(index) + timeRatio*( s(index + 1) - s(index) );
        else
            sNew(i) = s(index);
        end
    end
end

