function [x, y] = theta2xy(theta, s, l)
% Converts angular position to cartesin coordinates 
    x = s + l*sin(theta);
    y = -l*cos(theta);
end