function [safe, outsideLimits] = inJointLimits(qWaypoints)
% Checks whether all configurations are within limits of KUKA lbr iiwa7
%   
% Arguments:
%   qWaypoins (N X M array): M robot configurations in joint  
%                            space of a N DOF manipulator.
%
% Returns:
%   safe (bool): true if ALL rows represent configurations that are within
%               mechanical joint limits.
%   outsideLimits(M X N logical array): logical mask of qWaypoints, element
%                                       is true if it violates limit.

qWaypoints = qWaypoints.';

posLimits = deg2rad( [170, 120, 170, 120, 170, 120, 175] );
negLimits = -1 * posLimits;

M = size(posLimits, 1);
posLimitsArr = repmat(posLimits, M, 1);
negLimitsArr = repmat(negLimits, M, 1);

posLimitsResult = qWaypoints(qWaypoints > posLimitsArr);
negLimitsResult = qWaypoints(qWaypoints < negLimitsArr);

if ( isempty(posLimitsResult)  &&  isempty(negLimitsResult) )
    safe = true;
    outsideLimits = [];
else
    safe = false;
    posViolationIndeces = qWaypoints > posLimitsArr;
    negViolationIndeces = qWaypoints < negLimitsArr;
    outsideLimits = posViolationIndeces | negViolationIndeces;
end

end