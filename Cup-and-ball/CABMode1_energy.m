function [t, res, tE, thetaE, time_dds ] = CABMode1_energy(tspan, theta_init, s_init, g, l, m, Ed, k, kp, kd)
% Solves ODE for Mode #1: Pendulum mode, i.e. tension is >0.
% input is calculated using energy shaping
    
    tE = NaN;
    thetaE = NaN;
    time_dds = NaN(2, 10e4);
    cnt =1;
    %Opt = odeset('Events', @eventEndMode1);
    %[t, res, tE, thetaE] = ode45(@odefunMode1, tspan, [theta_init s_init], Opt); % for event
    [t, res] = ode45(@odefunMode1, tspan, [theta_init s_init]);                  % without event
    
    time_dds = rmmissing(time_dds, 2);
    [~,ind] = sort(time_dds(1,:));
    time_dds = time_dds(:,ind);


    function states_dot = odefunMode1(t, states)
        % initialize:
        states_dot = zeros(4,1);    % d/dt[theta, dtheta, s, ds]

        % calculate and record dds:
        if t <= 1%if abs( states(1) ) <= 0.08 && abs( states(2) ) <= 0.05
            dds = 0.4*sin(2*pi*t);
        else
            dds = k*states(2)*cos(states(1)) * ( 0.5*m*( (states(2)^2) *l^2) - m*g*l*cos(states(1)) -Ed ) -kp*states(3) -kd*states(4);
            dds_prev = time_dds(2,cnt-1);
            if abs(dds) > 3
                dds = dds_prev;
            end
        end

        time_dds(1,cnt) = t;
        time_dds(2,cnt) = dds;
        cnt = cnt + 1;

        % fill values
        states_dot(1) = states(2);
        states_dot(2) = -(dds/l)*cos(states(1)) - (g/l)*sin(states(1));
        states_dot(3) = states(4);
        states_dot(4) = dds;
    end

    function [choice,isterminal,direction] = eventEndMode1(t,states)
        % Should stop the solver if tension is lost
        % TO-DO: write a more physics-based tension condition! (see bachelor thesis fo recomendation)
        theta = states(1);
        dtheta = states(2);
        angle = abs(theta) - pi/2;
        s = states(3);
        ds = states(4);
        
        condForces = ( abs(theta) > pi/2 ) && ( (l*dtheta^2 - g*sin(angle) ) <= 0 );
        condDirec = sign(theta) == sign(ds);
        
        if condForces %&& condDirec
            disp([condForces, condDirec])
            verdict = 0;    % solver should be stopped
        else
            verdict = 1;    % solver should continue
        end
        choice = verdict;
        isterminal= 1;
        direction = 0;
    end
end