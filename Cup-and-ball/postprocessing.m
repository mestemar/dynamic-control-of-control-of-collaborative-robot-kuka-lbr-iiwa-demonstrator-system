%% AFTER ENERGY SHAPING
xs = s;


%% resample the input arrays
clc
sampling = 0.005;

[tNew, xsNew] = resampleArray(tTotal, xs,  sampling);        % the arguments tTotal and xs should be in workspace before running
%[tNew, ysNew] = resampleArray(tTotal, ys, sampling); 
%[~, dsNew] = resampleArray(tTotal, ds, sampling);
%[tNew2, ddsNew] = resampleArray(timeDds(1,:), timeDds(2,:), sampling);

% only a portion
Tresamppled = numel(tNew);
ratio = 1;
stopTime = round( ratio*Tresamppled);
tNew = tNew(1:stopTime);
xsNew = xsNew(1:stopTime);
%ysNew = ysNew(1:stopTime);

%% plot the comparison of new and old sampling
subplot(4,1,1);
scatter(tTotal, xs, 3);
hold on
scatter(tNew, xsNew, 3);
legend('old','new')
%subplot(4,1,2);
%scatter(tTotal, ys, 3);
%hold on
%scatter(tNew, ysNew, 3);
%legend('old','new')
%subplot(4,1,2);
%scatter(tTotal, ds, 3);
%hold on
%scatter(tNew, dsNew, 3);
%subplot(4,1,3);
%scatter(timeDds(1,:), timeDds(2,:), 3);
%hold on
%scatter(tNew2, ddsNew, 3);

%% get T arrays in time from the "sNew" array

arg1=KST.LBR7R800; % choose the robot iiwa7R800 or iiwa14R820
arg2=KST.Medien_Flansch_elektrisch; % choose the type of flange
Tef_flange=eye(4); % transofrm matrix of EEF with respect to flange
iiwa=KST("",arg1,arg2,Tef_flange); % create the object

N = numel(tNew);
q_start = [0; 0.071; 0; -1.3523; 0; 0.1301; 0];      %start pos1
%q_start = [0; -0.0530; 0; -1.7785; 0; -0.1722; 0];  %start pos2
[T0,~]=iiwa.gen_DirectKinematics(q_start);
T = repmat(T0,[1 1 N]);

for i=1:N
    T(2,4,i) = xsNew(i);    % xs in simulation is Y-axis of task space
    %T(3,4,i) = T0(3,4) + ysNew(i);    % ys in simulation is Z-axis of task space
end

%% compute IK

iter = 10;
q = zeros(7, N);
qin = q_start;
tic
for i=1:N
    q(:,i) = iiwa.gen_InverseKinematics(qin, T(:,:,i), iter, 0.01);
    qin = q(:,i);
end
toc

%% save the computed q to a csv file
q_csv = q.';
q_csv(abs(q_csv) < 1e-4) = 0;
writematrix(q_csv,'CpEnergyShapingQs3.csv')

%% from the computed q, compute FK and compare to simulation result
clc
Tcheck = zeros(4,4,N);

for i=1:N
    [Ti, ~] =iiwa.gen_DirectKinematics( q(:,i) );
    Tcheck(:,:,i) = Ti;
end

subplot(2,1,1)
scatter(tTotal, xs, 2);
hold on
y_planned = reshape( Tcheck(2,4,:), [1,N] ) ;
scatter(tNew, y_planned , 2)
title('planned origin position comparison')
legend('physics simulation','IK')
grid on

%subplot(2,1,2)
%scatter(tTotal, T0(3,4) + ys, 2);
%hold on
%z_planned = reshape( Tcheck(3,4,:), [1,N] ) ;
%scatter(tNew, z_planned , 2)
%title('planned origin position comparison')
%legend('physics simulation','IK')
%grid on
%% plot the trajectory for each joint in time
for i=1:7
    subplot(3,3,i);
    x = 1:1:N;
    scatter(x, q(i,:).',2);
    grid on
    title( join( ['joint A', string(i) ]) )
end

%% compute dq and ddq AND check for limits
q_deg = rad2deg(q);

% FORWARD DIFFERENTIATION
%dq_deg = diff(q_deg,1,2)/sampling;
%ddq_deg = diff(dq_deg,1,2)/sampling;

% SYMMETRIC DIFFERENTIATION
dq_deg = zeros(size(q_deg));
ddq_deg = zeros(size(q_deg));
for i=1:N
  if (i>1) && (i<N)
    dq_deg(:,i) = ( q_deg(:,i+1) - q_deg(:,i-1) )/ (2*sampling);
  elseif i == 1
    dq_deg(:,i) = ( q_deg(:,i+1) - q_deg(:,i) )/sampling;
  elseif i == N
     dq_deg(:,i) = ( q_deg(:,i) - q_deg(:,i-1) )/sampling;
  end
end

for i=1:N
  if (i>1) && (i<N)
    ddq_deg(:,i) = ( dq_deg(:,i+1) - dq_deg(:,i-1) )/ (2*sampling);
  elseif i == 1
    ddq_deg(:,i) = ( dq_deg(:,i+1) - dq_deg(:,i) )/sampling;
  elseif i == N
     ddq_deg(:,i) = ( dq_deg(:,i) - dq_deg(:,i-1) )/sampling;
  end
end

% check joint position, velocity and acceleration limits
clc
[safe,  ~] = inJointLimits(q)

velocity_limits = [98 98 100 130 140 180 180];  % deg.s^{-1}, KUKA manual
accel_limits = [490 490 500 650 700 900 900];  % deg.s^{-2}, research paper

for i=1:7
    subplot(4,4,i);
    x1 = 1:1:(N);
    scatter(x1, dq_deg(i,:).',3);
    hold on
    yline(velocity_limits(i),'Color','r');
    yline(-velocity_limits(i),'Color','r')
    title( join( ['joint velocity A', string(i) ]) );
    ylabel('deg.s{-1}')
    grid on
    hold off
end

for i=1:7
    subplot(4,4,i+7)
    x2 = 1:1:(N);
    scatter(x2, ddq_deg(i,:).', 3,'green');
    hold on
    yline(accel_limits(i),'Color','r');
    yline(-accel_limits(i),'Color','r')
    title( join( ['joint acceleration A', string(i) ]) );
    ylabel('deg.s^{-2}')
    grid on
    hold off
end


%% thesis - plotting
fsize = 17;
fig = figure;

for i=1:7
    j = 2*i-1;
    subplot(7,2,j);
    x1 = 0:sampling:((N*sampling)-sampling);
    scatter(x1, dq_deg(i,:).',3,"filled");
    ax = gca;
    ax.FontSize = 14; 
    hold on
    yline(velocity_limits(i),'Color','r');
    yline(-velocity_limits(i),'Color','r')
    plotTitle = 'joint velocity $\hat{\dot{q}}_' + string(i) + '$';
    title( plotTitle,"interpreter","latex" ,"FontSize",fsize);
    xlabel("time [s]","Interpreter","latex","FontSize",fsize)
    ylabel("[deg.s$^{-1}$]","interpreter","latex","FontSize",fsize)
    grid minor
    hold off
end

for i=1:7
    j = 2*i;
    subplot(7,2,j)
    x2 = 0:sampling:((N*sampling)-sampling);
    scatter(x2, ddq_deg(i,:).', 3, [0, 0.5, 0],'filled');
    ax = gca;
    ax.FontSize = 14; 
    hold on
    yline(accel_limits(i),'Color','r');
    yline(-accel_limits(i),'Color','r')
    plotTitle = 'joint acceleration $\hat{\ddot{q}}_' + string(i) + '$';
    title( plotTitle,"interpreter","latex" ,"FontSize",fsize);
    xlabel("time [s]","Interpreter","latex","FontSize",fsize)
    ylabel("[deg.s$^{-2}$]","interpreter","latex","FontSize",fsize)
    grid minor
    hold off
end

fig.Theme = "light"; 