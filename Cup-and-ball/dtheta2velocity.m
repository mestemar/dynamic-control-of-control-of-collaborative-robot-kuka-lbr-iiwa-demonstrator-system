function [dx, dy] = dtheta2velocity(dtheta, theta, l)
% Converts angular velocity to x and y components of tangential velocity
    norm = abs(dtheta*l);
    dx = norm.*cos(theta);
    dy = norm.*sin(theta);
    dx(dtheta < 0) = -dx(dtheta < 0);
    dy(dtheta < 0) = -dy(dtheta < 0);
end